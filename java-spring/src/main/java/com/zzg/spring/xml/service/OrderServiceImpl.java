package com.zzg.spring.xml.service;

import org.springframework.util.StringUtils;

import java.util.UUID;

public class OrderServiceImpl implements OrderService {

    private String name;
    private String type;
    private UserServiceImpl userService;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UserServiceImpl getUserService() {
        return userService;
    }

    public void setUserService(UserServiceImpl userService) {
        this.userService = userService;
    }

    public void initMethod() {
        System.out.printf("initMethod : className = %s; name = %s; type = %s;%n", this.getClass().getSimpleName(), name, type);
        if (this.type == null || this.type == "") {
            this.type = "int";
        }
    }

    public void destroyMethod() {
        System.out.printf("destroyMethod : className = %s; name = %s; type = %s;%n", this.getClass().getSimpleName(), name, type);
    }

    @Override
    public String createOrderNo() {
        if ("string".equalsIgnoreCase(type)) {
            return name + " - " + UUID.randomUUID().toString();
        }
        if ("int".equalsIgnoreCase(type)) {
            if (StringUtils.isEmpty(name)) {
                throw new RuntimeException("type is int, name must not null");
            }
            return name + " - " + System.currentTimeMillis();
        }
        throw new RuntimeException("error type defined");
    }

    @Override
    public String getOrderInfo(String orderNo) {
        return String.format("name = %s; type = %s; orderNo = %s; time = %d;", name, type, orderNo, System.currentTimeMillis());
    }

}
