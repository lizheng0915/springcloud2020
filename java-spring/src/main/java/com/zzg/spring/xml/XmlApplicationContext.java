package com.zzg.spring.xml;

import com.zzg.spring.util.ContextUtils;
import com.zzg.spring.xml.properties.PropertyComponent;
import com.zzg.spring.xml.service.OrderService;
import com.zzg.spring.xml.service.OrderServiceImpl;
import com.zzg.spring.xml.properties.PropertyServiceImpl;
import com.zzg.spring.xml.service.UserServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 使用解析xml的方式启动spring的容器
 */
public class XmlApplicationContext {

    public static void main(String[] args) {

        /**
         * 1. 通过指定xml配置文件加载bean信息
         * 2. 开始的跟目录是 classpath, 下面也可写成 classpath:applicationContext-service.xml
         * 3. 如果有多个配置文件可以使用数组传递 String[] 或者 String...
         * 4. 如果多个配置文件有一定规则，可以使用匹配符号进行处理，例如 : applicationContext-*.xml
         */
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-*.xml");
        ContextUtils.showContextInfo(context);

        System.out.println("============================================");
        OrderService orderServiceByName = context.getBean("orderService", OrderService.class);    // 根据beanName获取对象
        System.out.println("orderServiceByName = " + orderServiceByName);
        System.out.println(orderServiceByName.createOrderNo());

        OrderServiceImpl orderServiceByType = context.getBean(OrderServiceImpl.class);   // 根据beanType获取对象，当对象池包含多个相同类型的对象，无法确定注入哪个时，将报错
        System.out.println("orderServiceByType = " + orderServiceByType);
        System.out.println(orderServiceByType.createOrderNo());

        UserServiceImpl userService = context.getBean("userService", UserServiceImpl.class);
        System.out.println("userService = " + userService);
        OrderService orderService1 = userService.getOrderService();
        System.out.println("orderService1 = " + orderService1);

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        PropertyServiceImpl propertyService = context.getBean("propertyService", PropertyServiceImpl.class);
        System.out.println("propertyService = " + propertyService);

        PropertyComponent propertyComponent = context.getBean(PropertyComponent.class);
        System.out.println("propertyComponent.testValue() = " + propertyComponent.testValue());
    }

}
