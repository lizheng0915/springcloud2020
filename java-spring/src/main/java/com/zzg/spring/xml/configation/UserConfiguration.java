package com.zzg.spring.xml.configation;

import com.zzg.spring.xml.service.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfiguration {

    @Bean
    public UserServiceImpl getUserService() {
        return new UserServiceImpl();
    }

}
