package com.zzg.spring.xml.properties;

public class PropertyServiceImpl {

    private String author;
    private String date;
    private String name;
    private String type;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("author = %s; date = %s; name = %s; type = %s;", author, date, name, type);
    }

}
