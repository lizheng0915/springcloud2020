package com.zzg.spring.util;

import com.zzg.springcloud.common.util.DateUtils;
import org.springframework.context.ApplicationContext;

public class ContextUtils {


    public static void showContextInfo(ApplicationContext context) {
        System.out.println("==================context========start==========");
        System.out.println("context = " + context);
        System.out.println("context.getDisplayName() = " + context.getDisplayName());
        System.out.println("context.getApplicationName() = " + context.getApplicationName());
        System.out.println("context.getId() = " + context.getId());
        System.out.println("context.getStartupDate() = " + DateUtils.getDatetime(context.getStartupDate()));
        System.out.println("context.getBeanDefinitionCount() = " + context.getBeanDefinitionCount());
        String[] beanNames = context.getBeanDefinitionNames();
        for (String beanName : beanNames) {
            System.out.println("beanName = " + beanName);
        }

        System.out.println("==================context========end==========");
        System.out.println();
    }

}
