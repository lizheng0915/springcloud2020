package com.zzg.spring.annotation;


import com.zzg.spring.annotation.config.AppConfig;
import com.zzg.spring.annotation.properties.PropertyComponent;
import com.zzg.spring.util.ContextUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 使用注解方式启动spring, 使用 @Bean 来描述 bean 信息
 */
public class AnnotationApplicationContext {


    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        ContextUtils.showContextInfo(context);

        AppConfig appConfig = context.getBean("appConfig", AppConfig.class);
        System.out.println("appConfig = " + appConfig);

    }


}
