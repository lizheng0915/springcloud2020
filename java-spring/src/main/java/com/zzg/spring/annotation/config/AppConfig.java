package com.zzg.spring.annotation.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@ComponentScan(basePackages = "com.zzg.spring.annotation")
@PropertySource(value = "classpath:spring-dev.properties", encoding = "UTF-8")
public class AppConfig {




}
