package com.zzg.spring.annotation.properties;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 通过@Value获取properties中配置的环境变量
 *
 * @Value("${}") 表示一种占位符，根据操作系统的环境变量、JVM环境变量、properties文件，作为替换符的来源
 * @Value("${env.chrome.version}")   使用 $ 作为修饰符，则表示获取配置文件key定义的内容，老版本如果无法从配置文件中读取到对应的key，则会直接将 key 当做字符串直接赋值给属性，新版本的spring则会直接报错。
 * @Value("${project.date1:DefaultDateValue}")   使用 $ 作为前缀获取属性， : （冒号）后面表示如果无法从配置文件获取对应的key，则将默认字符串赋值给属性
 *
 *
 * @Value("name")                    不使用任何修饰符表示直接将 name 字符串赋值给属性
 * @Value("#{beanName}")             使用 # 作为修饰符，则表示从spring单例池中获取bean赋值给属性，相当于 @Autowired
 * @Value("#{'${project.list}'.split(',')}")    使用 # 作为修饰符，内部可以写表达式，java语言等。
 *
 * 可支持的数据类型有
 *
 * test.boolean=true
 * test.string=abc
 * test.integer=123
 * test.long=123
 * test.float=1.2345678123456
 * test.double=1.2345678123456
 * test.array=1,3,4,5,6,1,2,3
 * test.list=1,3,4,5,6,1,2,3
 * test.set=1,3,4,5,6,1,2,3
 * test.map={name:"张三", age:18}
 *
 * https://blog.csdn.net/weixin_42551921/article/details/105503458
 */
@Component
public class PropertyComponent {

    // 获取env.properties配置文件中的内容
    @Value("${env.chrome.version:defaultVersion}")
    private String chromeVersion;

    @Value("${project.author}")
    private String author;

    @Value("${project.date}")
    private String date;

    // 从JVM环境变量中取值
    @Value("${spring.profiles.active}")
    private String springProfilesActive;

    // 从操作系统环境变量中取值
    @Value("${JAVA_HOME}")
    private String javaHome;


    /**
     * 如果配置的属性不存在，则报错
     * 需要给默认值，如果不存在则使用默认值
     * 使用 : 后面的字符串表示默认值
     */
    @Value("${project.date1:DefaultDateValue}")
    private String date1;

    /**
     * 可支持 int、double、float、long基本数据类型
     * 不支持 date 时间类型
     * boolean 可以使用 true 和 false 表示
     */
    @Value("${project.age}")
    private int age;

    @Value("#{'${project.array}'.split(',')}")
    private String[] array;

    @Value("#{'${project.list}'.split(',')}")
    private List<String> list;

    @Value("#{'${project.set}'.split(',')}")
    private Set<String> set;

    @Value("#{${project.map}}")
    private Map<String, Object> map;

    public String testValue() {
        System.out.println("author = " + author);
        System.out.println("date = " + date);
        System.out.println("date1 = " + date1);
        System.out.println("age = " + age);
        System.out.println("array = " + array);
        System.out.println("list = " + list);
        System.out.println("set = " + set);
        System.out.println("map = " + map);
        System.out.println("map.get(\"name\") = " + map.get("name"));
        System.out.println("chromeVersion = " + chromeVersion);
        System.out.println("springProfilesActive = " + springProfilesActive);
        System.out.println("javaHome = " + javaHome);

        return String.format("UserComponent @Value : name = %s; date = %s; date1 = %s; age = %d;", author, date, date1, age);
    }

}
