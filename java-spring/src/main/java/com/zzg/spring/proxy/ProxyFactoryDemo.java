package com.zzg.spring.proxy;

import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.framework.ProxyFactory;

import java.lang.reflect.Method;

public class ProxyFactoryDemo {

    public static void main(String[] args) {

        LunaService service = new LunaService();
        service.testAdvice();
        System.out.println("service = " + service);
        System.out.println("=============================");

        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTarget(service);

        proxyFactory.addAdvice(new MethodBeforeAdvice() {
            @Override
            public void before(Method method, Object[] objects, Object o) throws Throwable {
                System.out.println("addAdvice~~~AOP~~~before~~~begin");
                System.out.println("method = " + method.getName());
                System.out.println("objects.size = " + objects.length);
                System.out.println("object = " + o);
                method.invoke(o, objects);
                System.out.println("addAdvice~~~AOP~~~before~~~end");
            }
        });

        LunaService proxyService = (LunaService) proxyFactory.getProxy();
//        System.out.println("proxyService = " + proxyService);
        proxyService.testAdvice();
    }


}
