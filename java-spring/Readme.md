
## Spring框架各个jar包的作用

#### Spring四个核心包
Spring四个核心包 : https://blog.csdn.net/qq_36806831/article/details/121889386

1. spring-beans :  spring IOC的基础实现，包含访问配置文件、创建和管理bean等，所有应用都用到。
2. spring-core : spring的核心工具包
3. spring-context : 在基础IOC功能上提供扩展服务
4. spring-expression : Spring的表达式语言

#### Spring jar包详解
关于spring包的详解 : https://blog.csdn.net/weixin_36338813/article/details/114557494
1. spring.jar : spring.jar是包含有完整发布的单个jar包，spring.jar 中包含除了 spring-mock.jar里所包含的内容外其它所有jar包的内容。
2. spring-core.jar
3. spring-beans.jar
4. spring-aop.jar
5. spring-context.jar
6. spring-dao.jar
7. spring-hibernate.jar
8. spring-jdbc.jar
9. spring-orm.jar
10. spring-remoting.jar
11. spring-support.jar
12. spring-web.jar
13. spring-webmvc.jar
14. spring-mock.jar


## spring 的 application.xml 配置文件
Spring5复习：当前applicationContext.xml中的所有标头 : https://blog.csdn.net/weixin_45492007/article/details/113801322

spring整合中application.xml配置 : https://blog.csdn.net/xuan_lu/article/details/79835075


## Spring
IOC/AOP

IOC 控制反转
DI 依赖注入
对象不需要自己创建，提前创建好放在spring容器内，使用的时候，直接获取


1. 来聊一聊spring
2. 说明bean的生命周期
3. 循环依赖
4. 三级缓存
5. FactoryBean和BeanFactory的区别
6. ApplicationContext和BeanFactory的区别
7. Spring中的设计模式

BeanFactory

BeanDefinition      -->     BeanDefinitionReader

DefaultSingletonBeanRegistry


ClassPathXmlApplicationContext

PostProcessor   后置处理器，增强器；目的是为了扩展，可以用于处理在properties中中配置的变量，替换

BeanFactoryPostProcessor




从源码分析@Qualifier,@Primary,@Priority的候选顺序
https://blog.csdn.net/you_and_dream/article/details/103247238