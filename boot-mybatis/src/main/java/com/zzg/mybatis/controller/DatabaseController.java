package com.zzg.mybatis.controller;


import com.alibaba.druid.pool.DruidDataSource;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@RestController
public class DatabaseController {

    @Autowired
    private DataSource dataSource;


    @PostConstruct
    public void initMethod() {
        try {
            Connection connection = dataSource.getConnection();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        if (dataSource instanceof DruidDataSource) {
            DruidDataSource druidDataSource = (DruidDataSource) dataSource;
            System.out.println("druidDataSource.getUrl() = " + druidDataSource.getUrl());
            System.out.println("druidDataSource.getPassword() = " + druidDataSource.getPassword());
            System.out.println("druidDataSource.getUsername() = " + druidDataSource.getUsername());
            System.out.println("druidDataSource.getDriverClassName() = " + druidDataSource.getDriverClassName());
            System.out.println("druidDataSource.getDriverClassLoader() = " + druidDataSource.getDriverClassLoader());
            System.out.println("druidDataSource.getCloseCount() = " + druidDataSource.getCloseCount());
            System.out.println("druidDataSource.getActiveCount() = " + druidDataSource.getActiveCount());
            System.out.println("=======================druidDataSource======================");
            System.out.println("druidDataSource.getInitialSize() = " + druidDataSource.getInitialSize());
            System.out.println("druidDataSource.getMaxActive() = " + druidDataSource.getMaxActive());
            System.out.println("druidDataSource.getMinIdle() = " + druidDataSource.getMinIdle());
            System.out.println("druidDataSource.getMaxWait() = " + druidDataSource.getMaxWait());
        }

        if (dataSource instanceof HikariDataSource) {
            HikariDataSource hikariDataSource = (HikariDataSource) dataSource;
            System.out.println("dataSource = " + dataSource);
            System.out.println("hikariDataSource = " + hikariDataSource);
            System.out.println("hikariDataSource.getDataSource() = " + hikariDataSource.getDataSource());
            System.out.println("hikariDataSource.getJdbcUrl() = " + hikariDataSource.getJdbcUrl());
            System.out.println("hikariDataSource.getPassword() = " + hikariDataSource.getPassword());
            System.out.println("hikariDataSource.getUsername() = " + hikariDataSource.getUsername());
            System.out.println("hikariDataSource.getDriverClassName() = " + hikariDataSource.getDriverClassName());
            System.out.println("hikariDataSource.getMaximumPoolSize() = " + hikariDataSource.getMaximumPoolSize());
            System.out.println("hikariDataSource.getMaxLifetime() = " + hikariDataSource.getMaxLifetime());
            System.out.println("hikariDataSource.getMinimumIdle() = " + hikariDataSource.getMinimumIdle());
        }

        System.out.println("1111");
    }


}
