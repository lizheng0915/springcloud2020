package com.zzg.thread.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 有界的任务队列: ArrayBlockingQueue
 * 无界的任务队列: LinkedBlockingQueue
 * 优先任务队列:   PriorityBlockingQueue
 */
public class BlockingQueueDemo {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<String> queue = new ArrayBlockingQueue<>(3);
        System.out.println("queue.getClass().getName() = " + queue.getClass().getName());
/*
        System.out.println("queue.add(\"a\") = " + queue.add("a"));
        System.out.println("queue.add(\"b\") = " + queue.add("b"));
        System.out.println("queue.add(\"c\") = " + queue.add("c"));
        System.out.println("queue.element() = " + queue.element());

        System.out.println("queue.remove() = " + queue.remove());
        System.out.println("queue.remove() = " + queue.remove("c"));
        System.out.println("queue.remove() = " + queue.remove());
        System.out.println("queue.element() = " + queue.element());
*/
        System.out.println(System.currentTimeMillis());
        System.out.println("queue.offer(\"a\") = " + queue.offer("a", 5, TimeUnit.SECONDS));
        System.out.println("queue.offer(\"a\") = " + queue.offer("a", 5, TimeUnit.SECONDS));
        System.out.println("queue.offer(\"a\") = " + queue.offer("a", 5, TimeUnit.SECONDS));
        System.out.println("queue.offer(\"x\") = " + queue.offer("x", 5, TimeUnit.SECONDS));
        System.out.println(System.currentTimeMillis());
    }

}
