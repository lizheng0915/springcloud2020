package com.zzg.thread.queue;

import com.zzg.springcloud.common.util.DateUtils;

import java.util.concurrent.*;

/**
 * @Author 张志刚
 * @Date 2022/1/4
 * @Description TODO
 * 多线程使用队列
 * 使用多线程往队列里面存放数据，另外的线程获取数据
 */
public class QueueWithThreadPoolDemo {

    // 这里最好根据系统负载量评估一个阈值，避免OOM问题 -- 定义一个长度100的阻塞队列
    public static BlockingQueue<String> arrayBlockingQueue = new ArrayBlockingQueue<>(10);

    // 定义一个固定核心线程数的线程池
    private final static ExecutorService fixedPool = Executors.newFixedThreadPool(20);

    public static void main(String[] args) throws InterruptedException {
        // 启动100个线程往队列里面放数据
        for (int i = 0; i < 100; i++) {
            final int tmpInt = i;
            fixedPool.execute(() -> {
                try {
                    TimeUnit.MILLISECONDS.sleep(2000);
                    String str = "Thread-Name--->" + tmpInt;
                    System.out.println("str-->" + tmpInt + " = " + arrayBlockingQueue.offer(str, 5, TimeUnit.SECONDS));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        while (!Thread.currentThread().isInterrupted()) {
            String res = arrayBlockingQueue.take();
            System.out.println(DateUtils.getDatetime() + "--->" + res);
        }
    }
}
