package com.zzg.thread.lock;


import java.util.concurrent.TimeUnit;

/**
 * 死锁示例
 * 编写一个能造成死锁的示例
 * <p>
 * 排查死锁:
 * 通过 jps -l 命令获取正在运行的JAVA进程以及进程ID
 * jps -l
 * <p>
 * 通过 jstack 44534 打印堆栈信息，看看是否有异常信息
 * jstack ${pid}
 * <p>
 * ## 如果获取如下信息，则表示线程AA和线程BB发生死锁 - Found 1 deadlock.
 * ## 发生死锁暂时无解，只能重启程序然后到业务代码里面进行处理，避免死锁现象。
 * Java stack information for the threads listed above:
 * ===================================================
 * "BB":
 * at zzg.jvm.thread.DeadLockDemo.lambda$main$1(DeadLockDemo.java:42)
 * - waiting to lock <0x00000000d6224ec8> (a java.lang.Object)
 * - locked <0x00000000d6224ed8> (a java.lang.Object)
 * at zzg.jvm.thread.DeadLockDemo$$Lambda$2/558638686.run(Unknown Source)
 * at java.lang.Thread.run(Thread.java:748)
 * "AA":
 * at zzg.jvm.thread.DeadLockDemo.lambda$main$0(DeadLockDemo.java:26)
 * - waiting to lock <0x00000000d6224ed8> (a java.lang.Object)
 * - locked <0x00000000d6224ec8> (a java.lang.Object)
 * at zzg.jvm.thread.DeadLockDemo$$Lambda$1/1747585824.run(Unknown Source)
 * at java.lang.Thread.run(Thread.java:748)
 * <p>
 * Found 1 deadlock.
 */
public class DeadLockDemo {

    public static void main(String[] args) {

//        Object objA = new Object();
//        Object objB = new Object();

        String objA = "A 线程先获得 objA 的锁，线程执行 1 秒钟后再去获取 objB 的锁";
        String objB = "A 线程先获得 objA 的锁，线程执行 1 秒钟后再去获取 objB 的锁";

        /**
         * A 线程先获得 objA 的锁，线程执行 1 秒钟后再去获取 objB 的锁
         * 而此时 objB 正在被线程 B拿着，并且在等待获取 objA 的锁
         */
        new Thread(() -> {
            synchronized (objA) {
                System.out.println(Thread.currentThread().getName() + "~~~获得锁 objA");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (objB) {
                    System.out.println(Thread.currentThread().getName() + "~~~获得锁 objB");
                }
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "AA").start();


        /**
         * B 线程先获得 objB 的锁，线程执行 1 秒钟后再去获取 objA 的锁
         * 而此时 objA 正在被线程A拿着，并且在等待获取 objB 的锁
         */
        new Thread(() -> {
            synchronized (objB) {
                System.out.println(Thread.currentThread().getName() + "~~~获得锁 objB");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (objA) {
                    System.out.println(Thread.currentThread().getName() + "~~~获得锁 objA");
                }
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "BB").start();

    }

}
