package com.zzg.thread.lock;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 题目要求: 多线程之间按照顺序环形执行 A==>B==>C==>A==>B==>C
 * AA打印5次， BB打印10次，CC打印15次
 * ......
 * 运行5轮
 * 使用ReentrantLock的Condition的精确唤醒功能实现
 */

public class CyclePrintLockDemo {

    private String conName = "A";
    private Lock lock = new ReentrantLock();
    private Condition ca = lock.newCondition();
    private Condition cb = lock.newCondition();
    private Condition cc = lock.newCondition();

    private void println(String name, int count) {
        System.out.println("进入println程序-->name = " + name + "; count = " + count);
        lock.lock();
        try {
            // 1. 判断
            while (!conName.equalsIgnoreCase(name)) {
                this.getCondition(name).await();
            }
            // 2. 执行
            for (int i = 1; i <= count; i++) {
                System.out.println(Thread.currentThread().getName() + "\t" + name + "\t" + i);
            }
            // 3. 唤醒
            this.conName = this.getNextName(name);
            this.getNextCondition(name).signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    /**
     * 后期需改造成枚举类型
     *
     * @param name
     * @return
     */
    private int getPrintCount(String name) {
        if ("A".equalsIgnoreCase(name)) return 2;
        if ("B".equalsIgnoreCase(name)) return 2;
        if ("C".equalsIgnoreCase(name)) return 2;
        return 1;
    }

    private String getNextName(String name) {
        if ("A".equalsIgnoreCase(name)) return "B";
        if ("B".equalsIgnoreCase(name)) return "C";
        if ("C".equalsIgnoreCase(name)) return "A";
        return "";
    }

    /**
     * 获取当前线程的条件
     */
    private Condition getCondition(String name) {
        if ("A".equalsIgnoreCase(name)) return ca;
        if ("B".equalsIgnoreCase(name)) return cb;
        if ("C".equalsIgnoreCase(name)) return cc;
        return null;
    }

    /**
     * 获取下一个线程的条件
     */
    private Condition getNextCondition(String name) {
        if ("A".equalsIgnoreCase(name)) return cb;
        if ("B".equalsIgnoreCase(name)) return cc;
        if ("C".equalsIgnoreCase(name)) return ca;
        return null;
    }

    public static void main(String[] args) {
        CyclePrintLockDemo demo = new CyclePrintLockDemo();
        /**
         * 定义三个线程，每个线程打印5轮，然后让由程序自动控制打印
         */
        List<String> list = Arrays.asList("A", "B", "C");
        for (String name : list) {
            new Thread(() -> {
                int count = demo.getPrintCount(name);
                for (int i = 0; i < 5; i++) {
                    demo.println(name, count);
                }
            }, name).start();
        }
    }

}
