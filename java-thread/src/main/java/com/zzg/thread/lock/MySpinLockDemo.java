package com.zzg.thread.lock;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 手撸一把自旋锁
 * <b>自旋锁</b> : 是指尝试获取锁的线程不会被阻塞，而是采用循环的方式去尝试获取锁，这样的好处是减少上下文的切换消耗，缺点是循环会消耗CPU。
 * 使用 CAS 理论进行编写自己的自旋锁
 */

public class MySpinLockDemo {

    /**
     * 定义一个原子引用对应，只不过这个原子引用里面放的是 Thread 对象
     * 在不往原子引用里面放对象的情况下，里面是 null
     */
    private AtomicReference<Thread> atomicReference = new AtomicReference<>();

    /**
     * 获取锁
     * 1. 获取当前线程的对象
     * 2. atomicReference.compareAndSet(null, thread)
     * 如果返回 true, 表示 atomicReference 还没放进入任何对象，是一把空锁，可以获取锁
     * 取反 --> 表示如果 atomicReference 不为空说明锁已经被占用，则不停的循环询问是否可以获取锁，直到有人释放锁
     */
    public void myLock() {
        Thread thread = Thread.currentThread();
        System.out.println(thread.getName() + "\t invoked myLock");
        while (!atomicReference.compareAndSet(null, thread)) {

        }
        System.out.println(thread.getName() + "\t get myLock");
    }

    /**
     * 释放锁
     * 1. 获取当前线程对象
     * 2. atomicReference.compareAndSet(thread, null)
     * 如果返回为 true, 表示 atomicReference 里面的对象是自己，表示自己拿着锁，可以进行释放
     */
    public void myUnlock() {
        Thread thread = Thread.currentThread();
        System.out.println(thread.getName() + "\t invoked myUnlock~~~~~~");
        atomicReference.compareAndSet(thread, null);
        System.out.println(thread.getName() + "\t release myLock");
    }

    public static void main(String[] args) {
        MySpinLockDemo spinLockDemo = new MySpinLockDemo();

        new Thread(() -> {
            spinLockDemo.myLock();
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                spinLockDemo.myUnlock();
            }
        }, "AA").start();

        new Thread(() -> {
            spinLockDemo.myLock();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                spinLockDemo.myUnlock();
            }
        }, "BB").start();
    }

}
