package com.zzg.thread.lock;

import java.util.concurrent.TimeUnit;

/**
 * Synchronous 死锁验证
 * 此死锁程序表明
 * this.getClass() 和 SynchronousDeadLockDemo.class 是同一把锁
 * synchronized 修饰的普通方法 和 SynchronousDeadLockDemo demo = new SynchronousDeadLockDemo(); 创建出来的 demo 是同一把锁
 */
public class SynchronousDeadLockDemo {

    public synchronized void hello1() {
        System.out.println(Thread.currentThread().getName() + "~~~拿到demo对象的锁");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (this.getClass()) {
            System.out.println(Thread.currentThread().getName() + "~~~拿到SynchronousDeadLockDemo.class对象的锁");
        }
    }

    public static synchronized void hello2() {
        System.out.println(Thread.currentThread().getName() + "~~~拿到SynchronousDeadLockDemo.class对象的锁");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("\t\t  hello2 --- begin");
        System.out.println("\t\t  hello2 --- " + Thread.currentThread().getName());
        System.out.println("\t\t  hello2 --- end");
    }

    public static void main(String[] args) {
        SynchronousDeadLockDemo demo = new SynchronousDeadLockDemo();
        new Thread(() -> {
            demo.hello1();
        }, "AA").start();

        new Thread(() -> {
            synchronized (SynchronousDeadLockDemo.class) {
                System.out.println(Thread.currentThread().getName() + "~~~拿到SynchronousDeadLockDemo.class对象的锁");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (demo) {
                    System.out.println(Thread.currentThread().getName() + "~~~拿到demo对象的锁");
                }
            }
        }, "BB").start();
    }

}
