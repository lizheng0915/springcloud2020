package com.zzg.thread.concurrent;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.util.RandomUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * https://blog.csdn.net/qq_38293564/article/details/80558157
 * <p>
 * Semaphore : 信号量，一个资源可供多个副本使用
 * Semaphore是用来保护一个或者多个共享资源的访问，Semaphore内部维护了一个计数器，其值为可以访问的共享资源的个数。
 * <p>
 * Semaphore semaphore = new Semaphore(7, true);   // 创建信号量对象，指定个数，指定模式
 * semaphore.acquire();  // 占有信号量
 * semaphore.release();  // 释放信号量
 */

public class SemaphoreDemo {

    private final static Integer count = 13;

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(count * count);
        /**
         * 7: 信号量个数，用于控制同时可以访问的个数
         * true: 表示使用，先进先出的公平模式进行共享 - 公平模式的信号量，先来的先获得信号量
         * 默认是false: 谁先抢到谁执行
         */
        Semaphore semaphore = new Semaphore(7, true);
        JSONArray resultArray = new JSONArray();
        Map<String, String> map = new ConcurrentHashMap<>();
        for (int i = 0; i < count * count; i++) {
            final int tt = i;
            new Thread(() -> {
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + "~~~抢到线程");
                    int rand = RandomUtils.getRandom(15);
                    JSONObject json = new JSONObject();
                    json.put("rand", rand);
                    resultArray.add(json);
                    map.put(tt + "", rand + "");
//                    TimeUnit.SECONDS.sleep(10);
                    System.out.println(Thread.currentThread().getName() + "~~~执行" + rand + "秒后，离开~~");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                    countDownLatch.countDown();
                }
            }, "Thread-Name-" + i).start();
        }
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName() + "~~~OVER");
        System.out.println(resultArray);
        System.out.println(resultArray.size());
        System.out.println(map);
        System.out.println(map.size());
    }

    // 并行x个线程，执行完毕后离开

    /**
     * @param parallelNum 并行线程数
     * @param totalNum    总需要执行数量
     */
    public static void semaphoreTest(int parallelNum, int totalNum) {
        Semaphore semaphore = new Semaphore(parallelNum);
        for (int i = 0; i < totalNum; i++) {
            new Thread(() -> {
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + "~~~抢到线程");
                    int rand = RandomUtils.getRandom(5);
                    TimeUnit.SECONDS.sleep(rand);
                    System.out.println(Thread.currentThread().getName() + "~~~执行" + rand + "秒后，离开~~");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }
            }, "Thread-Name-" + i).start();
        }
        System.out.println(Thread.currentThread().getName() + "~~~OVER");
    }

}
