package com.zzg.thread.concurrent;

import com.zzg.springcloud.common.util.PrintUtils;
import com.zzg.springcloud.common.util.SleepUtils;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * cyclic - 循环的
 * barrier - 障碍
 * CyclicBarrier 栅栏:
 * CyclicBarrier可以使一定数量的线程反复地在栅栏位置处汇集。
 * 当线程到达栅栏位置时将调用await方法，这个方法将阻塞直到所有线程都到达栅栏位置。如果所有线程都到达栅栏位置，那么栅栏将打开，此时所有的线程都将被释放，而栅栏将被重置以便下次使用。
 * 相当于是一组一组的执行
 * <p>
 * 参考文章: https://blog.csdn.net/qq_38293564/article/details/80558157
 */
public class CyclicBarrierDemo {

    /**
     * 表示屏障拦截的线程数量
     * 每个线程使用await()方法告诉CyclicBarrier我已经到达了屏障，然后当前线程被阻塞。
     */
    private final static Integer parties = 6;

    public static void main(String[] args) {

        /**
         * CyclicBarrier(int parties, Runnable barrierAction)
         * 需要传入一个数值和一个接口，当数值达到期望后执行的内容
         * 最好是使用 lamada 表达式，当然也可以使用匿名内部类，也可以使用外部定义类然后创建对象传入
         */
        CyclicBarrier cyclicBarrier = new CyclicBarrier(parties, () -> {
            System.out.println(Thread.currentThread().getName() + "~~~开门~~~");
        });

        for (int i = 0; i < 22; i++) {
            final int tempint = i;
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "~~~" + tempint + "~~~" + cyclicBarrier.getNumberWaiting());
                try {
                    /**
                     * cyclicBarrier.await(); 阻塞、等着的意思
                     * 等阻塞或者等待的线程数达到指定次数后，执行主线程方法
                     */
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }, "Thread-Name-" + i).start();
            SleepUtils.sleepMilliseconds(100);
        }

        PrintUtils.printThreadAndMessage(cyclicBarrier.getParties());
        PrintUtils.printThreadAndMessage(cyclicBarrier.isBroken());
        cyclicBarrier.reset();
    }

}
