package com.zzg.thread.concurrent;

import java.util.concurrent.TimeUnit;

/**
 * volatile 关键字示例
 * 1. 保证可见性 - 如何保证可见性 :
 * 被 votalite 标记的变量被复制到工作内存后，要求线程每次获取该变量数据的时候，都直接从主内存中获取，而不是自己的工作内存。
 * 但是每次都从主内存中获取所以会导致程序性能的降低。
 * 2. 不支持原子性 - 也就是说多个线程同时修改的时候，无法保证其原子性，因为在进行操作的时候，是先获取数据，再修改数据，这是两步操作，而这两个在volatile修饰下是无法保证原子的。
 * 3. 禁止指令重排 - 基本保证有序性。如何保证 : volatile 标记的变量，会警告JIT编译器，当前标记了volatile 的变量可能会被其他线程修改，不要执行任何可能影响其访问顺序的优化。
 */
public class VolatileDemo {

    private static int mm = 0;

    public static void main(String[] args) {
        volatileTest();
    }

    /**
     * volatile 可见性测试
     * 两个线程 - 主线程 和 主线程下新建的线程
     * 1. 主线程获取mm的值，并打印
     * 2. 主线程新建线程，新建线程对mm进行修改操作多次
     * 3. 主线程等自己创建的子线程执行完毕后，打印mm的结果，显示的是修改之后的数据
     * <p>
     * 当前示例有问题，即便 mm 不适用 volatile 修饰，也仍然显示的是修改后的数据
     */
    public static void volatileTest() {
        System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->volatileTest begin-->mm=" + mm);
        new Thread(() -> {
            System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->begin-->mm=" + mm);
            for (int i = 0; i < 3; i++) {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mm += 60;
            }
            System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->end-->mm=" + mm);
        }, "Thread-name-aaa").start();

        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println("================最终结果================");
        System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->volatileTest end-->mm=" + mm);
    }
}
