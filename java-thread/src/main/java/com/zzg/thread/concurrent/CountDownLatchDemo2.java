package com.zzg.thread.concurrent;

import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.util.RandomUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * 倒计时计数器
 * 从某一个数字开始，线程每操作一次计数器减一，直到最后
 * <p>
 * 一个方法内，多线程执行
 * 1、 修改数据
 * 2、 汇总处理结果
 * <p>
 * 示例文档: https://www.cnblogs.com/dafanjoy/p/14505058.html
 */

public class CountDownLatchDemo2 {

    public static void main(String[] args) throws InterruptedException {
        countDownLatchTest();
    }

    /**
     * eg: 需要连接数据库查询多个表的数据，同时把结果汇总，在查询的过程中，将查询结果的数量汇总计数
     *
     * @throws InterruptedException
     */
    public static void countDownLatchTest() throws InterruptedException {
        // 需要提前知道要到哪些表中获取数据
        List<String> tableNameList = Arrays.asList("tableName1,tableName2,tableName3,tableName4,tableName5".split(","));

        // 创建倒计时计数器，初始值为需要查询的表的数量
        CountDownLatch countDownLatch = new CountDownLatch(tableNameList.size());

        // 创建线程安全的 list 对象，用于汇总存储多线程查询到的数据
        List<JSONObject> resultList = Collections.synchronizedList(new ArrayList<>());

        // 创建线程安全的计数器对象，用于汇总查询出来的数量
        AtomicInteger atomicInteger = new AtomicInteger();

        // 循环创建线程 - 执行查询
        for (String tableName : tableNameList) {
            new Thread(() -> {
                // 创建 json 对象，对其赋值，模拟从数据库查询的动作，并通过 resultList 收集结果
                JSONObject json = new JSONObject();
                json.put("threadName", Thread.currentThread().getName());
                json.put("tableName", tableName);
                resultList.add(json);

                // 随机一个数据，模拟获取查询出来的数据量，通过 AtomicInteger 进行汇总
                int rand = RandomUtils.getRandom(100);
                atomicInteger.addAndGet(rand);

                // 打印线程信息 - 打印countDownLatch当前值，同时对 countDownLatch 减一
                System.out.println(Thread.currentThread().getName() + "~~~倒计时~~~count=" + countDownLatch.getCount());
                countDownLatch.countDown();

            }, "Thread-Name-" + tableName).start();
        }
        /**
         * 等待计数器执行结束 - 相当于值为0
         * 相当于
         * while (countDownLatch.getCount() != 0) {
         *      Thread.yield();
         * }
         */
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName() + "~~~OVER");
        System.out.println(Thread.currentThread().getName() + "~~~atomicInteger = " + atomicInteger.get());
        System.out.println(Thread.currentThread().getName() + "~~~countDownLatch = " + countDownLatch.getCount());
        System.out.println(Thread.currentThread().getName() + "~~~resultList = " + resultList);
        System.out.println(Thread.currentThread().getName() + "~~~resultList.size = " + resultList.size());
    }

}
