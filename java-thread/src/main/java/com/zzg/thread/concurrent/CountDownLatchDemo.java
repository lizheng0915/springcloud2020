package com.zzg.thread.concurrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * 倒计时计数器
 * 从某一个数字开始，线程每操作一次计数器减一，直到最后
 * <p>
 * countDownLatch.await(); 表示执行完毕，倒计时结束
 */

public class CountDownLatchDemo {

    private final static Integer count = 6;

    public static void main(String[] args) throws InterruptedException {
//        countDownLatchTest();
        atomicIntegerIncrement();
    }

    public static void countDownLatchTest() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(count);
        for (int i = 0; i < count; i++) {
            new Thread(() -> {
                /**
                 * 能保证顺序执行，以此递减，但是无法保证在执行 countDownLatch.getCount() 方法的时候是最新的数据
                 */
//                System.out.println(Thread.currentThread().getName() + "~~~倒计时~~~");
                System.out.println(Thread.currentThread().getName() + "~~~倒计时~~~count=" + countDownLatch.getCount());
                countDownLatch.countDown();
                /**
                 * countDownLatch.countDown() + countDownLatch.getCount()
                 * 组成的代码块无法保证原子，所有有时候看到打印顺序跟期望的并不一样
                 */
//                System.out.println(Thread.currentThread().getName() + "~~~倒计时~~~count=" + countDownLatch.getCount());
            }, "Thread-Name-CountDownLatch-" + i).start();
        }
        //
        /**
         * 等待计数器执行结束 - 相当于值为0
         * 相当于
         * while (countDownLatch.getCount() != 0) {
         *      Thread.yield();
         * }
         */
//        countDownLatch.await();

        while (countDownLatch.getCount() != 0) {
            Thread.yield();
        }

        System.out.println(Thread.currentThread().getName() + "~~~OVER");
    }

    /**
     * Thread-Name-AtomicInteger-0~~~离开~~~count=0
     * Thread-Name-AtomicInteger-1~~~离开~~~count=0
     * Thread-Name-AtomicInteger-2~~~离开~~~count=0
     * Thread-Name-AtomicInteger-5~~~离开~~~count=2
     * Thread-Name-AtomicInteger-3~~~离开~~~count=4
     * Thread-Name-AtomicInteger-4~~~离开~~~count=3
     * main~~~关门
     * <p>
     * 如果打印这种结果也说明正常
     * AtomicInteger 只能保证一个方法是原子操作, 无法保证代码块的原子性, 所以在执行get()后再执行atomicInteger.getAndIncrement()
     * 期间，可能已经有多个线程在执行get方法和打印语句
     */
    public static void atomicIntegerIncrement() {
        AtomicInteger atomicInteger = new AtomicInteger();
        for (int i = 0; i < count; i++) {
            new Thread(() -> {
                /**
                 * 打印出来的 atomicInteger.get() 值跟期望的不符，也是因为多线程获取数据的原因
                 * 只能通过CAS保证 atomicInteger.getAndIncrement() 的原子性
                 */
                System.out.println(Thread.currentThread().getName() + "~~~离开~~~count=" + atomicInteger.get());
                atomicInteger.getAndIncrement();
            }, "Thread-Name-AtomicInteger-" + i).start();
        }
        /**
         * 当主线程抢到CPU执行权限后判断 : 如果计数器的数值还没达到想要的结果, 说明还有线程没有执行完毕, 从而让出CPU执行权, 从执行线程变为可执行线程
         * 由CPU再次选择 - 不过有可能主线程再次抢到
         */
        while (atomicInteger.get() != count) {
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName() + "~~~关门");
    }

}
