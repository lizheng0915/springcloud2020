package com.zzg.thread.threadpool;


import com.zzg.thread.threadlocal.ThreadLocalUtils;
import lombok.SneakyThrows;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * main 线程中不关闭线程池则线程一直处于运行状态，不会完结
 * 测试在非main方法中的情况
 */
public class ThreadPoolDemo2 {


    public static void main(String[] args) throws InterruptedException {
        ThreadPoolDemo2 demo2 = new ThreadPoolDemo2();
        String res = demo2.executePool();
        System.out.println(res);
    }

    public String executePool() throws InterruptedException {
        ExecutorService fixedPool = Executors.newFixedThreadPool(3);
        for (int i = 1; i <= 10; i++) {
            TimeUnit.MILLISECONDS.sleep(100);
            final int tmpint = i;
            fixedPool.execute(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName() + "~~" + tmpint + "~~~" + ThreadLocalUtils.get());
                    TimeUnit.SECONDS.sleep(4);
                }
            });
        }
        System.out.println("=============线程执行完毕==============");
        System.out.println("fixedPool.isShutdown() = " + fixedPool.isShutdown());
        System.out.println("fixedPool.isTerminated() = " + fixedPool.isTerminated());
//        fixedPool.shutdown();
        List<Runnable> list = fixedPool.shutdownNow();
        for (Runnable rr : list) {
            Thread t = new Thread(rr);
            t.start();
        }
        System.out.println("fixedPool.isShutdown() 2= " + fixedPool.isShutdown());
        System.out.println("fixedPool.isTerminated() 2= " + fixedPool.isTerminated());
        System.out.println("=============线程执行完毕 shutdown==============");
        return "SUCCESS";
    }

}
