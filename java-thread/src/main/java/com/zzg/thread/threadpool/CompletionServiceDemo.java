package com.zzg.thread.threadpool;


import com.zzg.springcloud.common.util.PrintUtils;
import com.zzg.springcloud.common.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * https://mp.weixin.qq.com/s/rvN5oVsMPTmHdSpNIO9JSA
 * <p>
 * 多线程使用不当引发的OOM - 原因是使用了 ExecutorCompletionService，但是没有调用 take，poll 方法。
 * 最终导致执行结果无法释放，一直堆积引发的堆内存溢出
 * <p>
 * 1. ExecutorCompletionService 是 CompletionService 接口的实现类
 * 2. ExecutorCompletionService 的构造必须一个线程池对象，默认使用的队列是 LinkedBlockingQueue，不过可以自行指定队列
 * 3. submit 任务提交的两种方式，都是有返回值的
 * 4. 对比 ExecutorService 和 ExecutorCompletionService submit 方法，可以看出区别。
 * 5. ExecutorCompletionService 的 submit 方法在 executor.execute的时候，将任务传到了 QueueingFuture 对象中
 * 6. 最终区别是 ExecutorService的submit 执行完后，将任务结果直接返回，而ExecutorCompletionService的submit执行完后，将任务结果放入一个队列里面
 * 7. 但是如果不调用service获取返回结果的方法，那么队列中的数据会越积越多，最终导致OOM
 * 8. 注意: 当只有调用了 ExecutorCompletionService 下面的 3 个方法的任意一个时，阻塞队列中的 task 执行结果才会从队列中移除掉，释放堆内存。
 * Future take(), Future poll(), Future poll(time, unit);
 * <p>
 * ====作用====
 * ExecutorCompletionService的特性
 * 普通的 ExecutorService 在将任务submit到线程池之后，想要获取对应的结果
 * 那必须等所有的任务执行完毕后，才能按照List<Future>里面的顺序获取结果，即便第一个提交的执行的时间最长，后面的也依然要等第一个执行完毕后才能得到结果
 * <p>
 * 而如果使用 ExecutorCompletionService 执行任务的话，可以提前获取先执行完成的任务。
 * 哪个任务执行完成哪个结果就可以先获取得到，这样可以提高后续任务的执行效率。
 * <p>
 * 1. ExecutorCompletionService 需要依赖一个线程池参数才能创建
 * 2. CompletionService 主要用于接收返回值的异步方法，所以不支持 execute 方法提交任务，只支持 submit 方法提交任务
 */
public class CompletionServiceDemo {

    private ExecutorService threadPool = Executors.newFixedThreadPool(5);
    private CompletionService<String> completionService = new ExecutorCompletionService<>(threadPool);

    private AtomicInteger atomicInteger = new AtomicInteger();

    public static void main(String[] args) throws Exception {
        CompletionServiceDemo demo = new CompletionServiceDemo();
        /*while (true) {
            demo.errorMethod();
            TimeUnit.MILLISECONDS.sleep(100);
        }*/

//        demo.onePushOnePollMethod();
        demo.realUsedMethod();
    }


    /**
     * 有问题的方法演示，事故代码模拟如下
     * 问题原因: 使用 ExecutorCompletionService
     * 1. 创建 CompletionService 对象，泛型中的内容是线程执行后返回的内容
     * 2. 设置当前运行程序堆内存小一点，以便引发 OOM， -Xms10M -Xmx10M  -XX:+PrintGCDetails
     * 3.  -XX:+PrintGCDetails 是打印GC日志的，可以取消
     * 4. 通过 service 调用 submit方法，不获取返回值，最终导致异常
     */
    public void errorMethod() {
        CompletionService<String> service = new ExecutorCompletionService<>(threadPool);
        for (int i = 0; i < 5; i++) {
            service.submit(() -> {
//                String res = RandomUtils.getRandomCode(1024);
                StringBuffer sb = new StringBuffer();
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                sb.append(UUID.randomUUID().toString());
                System.out.println(atomicInteger.getAndIncrement());
                return sb.toString();
            });
        }
    }

    /**
     * 如果 ExecutorCompletionService 是全局对象的情况下，使用 completionService.take().get(); 来获取数据
     * 可能导致 A 线程提交的数据被 B 线程获取，从而导致数据错乱
     */
    public void onePushOnePollMethod() throws InterruptedException {

        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                completionService.submit(() -> {
                    String res = UUID.randomUUID().toString();
                    return res;
                });
            }
        }).start();

        TimeUnit.SECONDS.sleep(1);

        new Thread(() -> {
            try {
                String res = completionService.take().get();
                System.out.println(res);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }).start();

    }


    /**
     * 真实业务场景下的使用方法
     * 1. https://mp.weixin.qq.com/s/rvN5oVsMPTmHdSpNIO9JSA 文章中的例子很好的诠释了 ExecutorCompletionService 作用
     * 2. 但是在真实的业务场景下，线程池和 ExecutorCompletionService 都不会是局部变量，都需要定义成全局变量进行使用
     * 3. 所以在实际的业务场景下是无法使用 ExecutorCompletionService 实例直接调用 completionService.take().get(); 方法来获取结果的，
     * 否则会导致数据错乱，很可能把其他线程提交的任务结果拿错了
     * 4. 所以，仍然是需要通过 Future<T> 来接收，然后再统一获取数据
     * 5. 文章中的例子就不再重现，直接编写实际场景中可能用到的例子
     * 6.
     */
    public void realUsedMethod() throws Exception {
        List<Future<String>> futureList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            final int tempInt = i;
            Future<String> future = completionService.submit(() -> {
                int random = RandomUtils.getRandom(1000, 10000);
                PrintUtils.format("Begin : index = %d; random = %d;", tempInt, random);
                TimeUnit.MILLISECONDS.sleep(random);
                PrintUtils.format("End   : index = %d; random = %d;", tempInt, random);
                return String.format("OVER  : index = %d; random = %d;", tempInt, random);
            });
            futureList.add(future);
        }

        TimeUnit.SECONDS.sleep(1);

        for (int i = 0; i < 4; i++) {
            String res = completionService.take().get();
            System.out.println(res);
        }

        /*for (Future<String> future : futureList) {
            String res = future.get();
            System.out.println(res);
        }*/

    }

}
