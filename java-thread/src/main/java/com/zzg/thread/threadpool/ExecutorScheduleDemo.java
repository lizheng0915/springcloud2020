package com.zzg.thread.threadpool;

import com.sun.istack.internal.NotNull;
import com.zzg.springcloud.common.util.DateUtils;
import com.zzg.springcloud.common.util.RandomUtils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * java.util.concurrent.ScheduledExecutorService 继承 java.util.concurrent.ExecutorService
 * 是一个可以指定延迟执行或者定时执行的线程池
 * <p>
 * newSingleThreadScheduledExecutor 和 newScheduledThreadPool 区别就是可用线程数量不同而已
 * <p>
 * 其他各个 schedule 的方法的区别，参考其他类
 */
public class ExecutorScheduleDemo {

    /**
     * 单线程延迟定时执行线程池 - 同时只有一个线程在执行任务
     */
    private static ScheduledExecutorService singleSchedulePool = Executors.newSingleThreadScheduledExecutor();

    /**
     * 固定线程数量的
     */
    private static ScheduledExecutorService schedulePool = Executors.newScheduledThreadPool(3);


    public static void main(String[] args) {
        newSingleThreadScheduledExecutorTest();
    }

    /**
     * 同时只有一个线程在执行任务，schedule 之后任务并不是到任务池等待
     * 而是进入一个倒计时等待队列，等时间到了之后再提交给线程池进行执行任务
     * 下面示例中 - 首先提交的任务，delay 时间长，最后提交的任务，delay的时间短；所以最后提交的任务先执行完毕
     * 同时在方法内部设置休眠时间，说明只有一个线程在执行任务，delay时间到了之后提交给线程池，然后就开始等待执行
     */
    public static void newSingleThreadScheduledExecutorTest() {
        for (int i = 0; i < 10; i++) {
            final int tempInt = i;
            int delay = 11 - i;
            singleSchedulePool.schedule(() -> {
                try {
                    int rand = RandomUtils.getRandom(2000);
                    TimeUnit.MILLISECONDS.sleep(rand);
                    System.out.println(String.format("threadName = %s; index = %d; sleep = %5d; datetime = %s;", Thread.currentThread(), tempInt, rand, DateUtils.getDatetime()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, delay, TimeUnit.SECONDS);
        }
    }

}
