package com.zzg.thread.threadpool;

import com.zzg.springcloud.common.util.DateUtils;
import com.zzg.thread.threadlocal.ThreadLocalUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPoolDemo3 {

    private final static ExecutorService fixedPool = Executors.newFixedThreadPool(3);
    private final static AtomicInteger ATOMIC_INTEGER = new AtomicInteger();

    public static void main(String[] args) {

        for (int i = 1; i <= 10; i++) {
            System.out.println(Thread.currentThread().getName() + "\t\t" + ThreadLocalUtils.get());
            new Thread(() -> {
                for (int j = 1; j <= 5; j++) {
                    final int tmpj = j;
                    System.out.println(Thread.currentThread().getName() + "\t\t" + ThreadLocalUtils.get());
                    fixedPool.execute(() -> {
                        int index = ATOMIC_INTEGER.incrementAndGet();
                        System.out.println("index = " + index + "\tj = " + tmpj + "\t" + DateUtils.getDatetime() + "\t" + Thread.currentThread().getName() + "\t" + ThreadLocalUtils.get());
                        try {
                            TimeUnit.SECONDS.sleep(2);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                }
            }, "Thread-Name-" + i).start();
        }

    }

}
