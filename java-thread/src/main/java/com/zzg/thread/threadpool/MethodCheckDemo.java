package com.zzg.thread.threadpool;

import com.zzg.springcloud.common.util.RandomUtils;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 配合 MethodProvideDemo 验证多线程下各种方法的安全性
 */
public class MethodCheckDemo {

    private static ExecutorService fixedPool = Executors.newFixedThreadPool(50);

    private static MethodProvideDemo staticProvideDemo = new MethodProvideDemo();

    private static List<Map<String, Object>> list = Collections.synchronizedList(new ArrayList<>());

    private static AtomicInteger atomicInteger = new AtomicInteger();

    public static void main(String[] args) throws InterruptedException {
//        checkStaticMethod();
//        checkSimpleDateFormat();

        checkSimpleDateFormatFinal();

        while (true) {
            if (atomicInteger.get() == 10000) break;
        }
        fixedPool.shutdown();

        System.out.println("==========OVER==========" + atomicInteger.get());

        Map<String, Integer> resMap = new HashMap<>();
        System.out.println(list.size());
        for (Map<String, Object> map : list) {
            Long time = Long.parseLong(map.get("time").toString());
            String realFormat = staticProvideDemo.syncFormat(time);
            for (String key : map.keySet()) {
                if (key.startsWith("res")) {
                    String res = map.get(key).toString();
                    if (!realFormat.equalsIgnoreCase(res)) {
                        int count = resMap.containsKey(key) ? resMap.get(key) + 1 : 1;
                        resMap.put(key, count);
                    }
                }
            }
        }

        System.out.println(resMap);
    }

    /**
     * 验证静态方法
     */
    public static void checkStaticMethod() {
        MethodProvideDemo methodProvideDemo = new MethodProvideDemo();
        for (int i = 0; i < 10000; i++) {
            final int tempInt = i;
            fixedPool.execute(() -> {
                MethodProvideDemo innerProvideDemo = new MethodProvideDemo();
                int a = RandomUtils.getRandom(100);
                int b = RandomUtils.getRandom(100);

                int res1 = staticProvideDemo.add(a, b);
                int res2 = staticProvideDemo.syncAdd(a, b);

                int res3 = methodProvideDemo.add(a, b);
                int res4 = methodProvideDemo.syncAdd(a, b);

                int res5 = innerProvideDemo.add(a, b);
                int res6 = innerProvideDemo.syncAdd(a, b);

                int res7 = MethodProvideDemo.staticAdd(a, b);
                int res8 = MethodProvideDemo.syncStaticAdd(a, b);

                if (res1 == res2 && res2 == res3 && res3 == res4 && res4 == res5 && res5 == res6 && res6 == res7 && res7 == res8) {
                    if (tempInt % 100 == 0) {
                        System.out.println(String.format("index = %4d;", tempInt));
                    }
                } else {
                    System.out.println(String.format("static add : index = %4d; a = %d; b = %d; " +
                                    "res1 = %d; res2 = %d; res3 = %d; res4 = %d; res5 = %d; res6 = %d; res7 = %d; res8 = %d; ",
                            tempInt, a, b, res1, res2, res3, res4, res5, res6, res7, res8));
                }
            });
        }
    }


    /**
     * 验证静态方法
     */
    public static void checkSimpleDateFormat() {
        MethodProvideDemo methodProvideDemo = new MethodProvideDemo();
        for (int i = 0; i < 10000; i++) {
            final int tempInt = i;
            fixedPool.execute(() -> {
                long time = System.currentTimeMillis() - RandomUtils.getRandom(1000000000);
                MethodProvideDemo innerProvideDemo = new MethodProvideDemo();

                String res1 = staticProvideDemo.format(time);
                String res2 = staticProvideDemo.syncFormat(time);

                String res3 = methodProvideDemo.format(time);
                String res4 = methodProvideDemo.syncFormat(time);

                String res5 = innerProvideDemo.format(time);
                String res6 = innerProvideDemo.syncFormat(time);

                String res7 = MethodProvideDemo.staticFormat(time);
                String res8 = MethodProvideDemo.syncStaticFormat(time);

                Set<String> set = new HashSet<>();
                set.add(res1);
                set.add(res2);
                set.add(res3);
                set.add(res4);
                set.add(res5);
                set.add(res6);
                set.add(res7);
                set.add(res8);

                if (set.size() == 1) {
                    if (tempInt % 100 == 0) {
                        System.out.println(String.format("index = %4d;", tempInt));
                    }
                } else {
                    Map<String, Object> map = new HashMap<>();
                    map.put("index", tempInt);
                    map.put("time", time);

                    map.put("res1", res1);
                    map.put("res2", res2);
                    map.put("res3", res3);
                    map.put("res4", res4);
                    map.put("res5", res5);
                    map.put("res6", res6);
                    map.put("res7", res7);
                    map.put("res8", res8);

                    list.add(map);
                    System.out.println(String.format("static add : index = %4d;  time = %d;  " +
                                    "res1 = %s; res2 = %s; res3 = %s; res4 = %s; res5 = %s; res6 = %s; res7 = %s; res8 = %s; ",
                            tempInt, time, res1, res2, res3, res4, res5, res6, res7, res8));
                }
                atomicInteger.addAndGet(1);
            });
        }
    }


    public static void checkSimpleDateFormatFinal() {
        MethodProvideDemo methodProvideDemo = new MethodProvideDemo();
        for (int i = 0; i < 10000; i++) {
            final int tempInt = i;
            fixedPool.execute(() -> {
                long time = System.currentTimeMillis() - RandomUtils.getRandom(1000000000);
                MethodProvideDemo innerProvideDemo = new MethodProvideDemo();

                String realRes = innerProvideDemo.format(time);            // 只有 realRes res6 是绝对安全的，再用来单独验证其他的方法，对象单独创建，那么对应的属性也是有多个的，不存在共享问题
//                String res6 = innerProvideDemo.syncFormat(time);

//                String res1 = staticProvideDemo.format(time);              // 绝对不安全
//                String res1 = staticProvideDemo.syncFormat(time);          // 相对安全，单独比对的时候线程安全
//                String res1 = methodProvideDemo.format(time);              // 绝对不安全
//                String res1 = methodProvideDemo.syncFormat(time);          // 全局只一个对象在使用，看起来安全
//                String res1 = MethodProvideDemo.staticFormat(time);        // 绝对不安全
                String res1 = MethodProvideDemo.syncStaticFormat(time);      // 相对安全

                if (realRes.equalsIgnoreCase(res1)) {
                    if (tempInt % 500 == 0) {
                        System.out.println(String.format("index = %4d;", tempInt));
                    }
                } else {
                    Map<String, Object> map = new HashMap<>();
                    map.put("index", tempInt);
                    map.put("time", time);

                    map.put("res1", res1);
                    map.put("realRes", realRes);

                    list.add(map);
                    System.out.println(String.format("static add : index = %4d;  time = %d; realRes = %s; res1 = %s; ", tempInt, time, realRes, res1));
                }
                atomicInteger.addAndGet(1);
            });
        }
    }

}
