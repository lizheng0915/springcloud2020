package com.zzg.thread.threadpool;

import com.zzg.thread.threadlocal.ThreadLocalUtils;
import lombok.SneakyThrows;

import java.util.concurrent.*;

/**
 * 线程池的使用
 * 在《阿里巴巴java开发手册》中指出了线程资源必须通过线程池提供，不允许在应用中自行显示的创建线程，
 * 这样一方面是线程的创建更加规范，可以合理控制开辟线程的数量；
 * 另一方面线程的细节管理交给线程池处理，优化了资源的开销。
 * 而线程池不允许使用Executors去创建，而要通过ThreadPoolExecutor方式，
 * 这一方面是由于jdk中Executor框架虽然提供了如newFixedThreadPool()、newSingleThreadExecutor()、
 * newCachedThreadPool()等创建线程池的方法，但都有其局限性，不够灵活；
 * <p>
 * 另外由于前面几种方法内部也是通过ThreadPoolExecutor方式实现，
 * 使用ThreadPoolExecutor有助于大家明确线程池的运行规则，
 * 创建符合自己的业务场景需要的线程池，避免资源耗尽的风险。
 * <p>
 * https://www.cnblogs.com/dafanjoy/p/14505058.html
 */
public class ExecutorThreadPoolDemo {

    public static void main(String[] args) {
        ExecutorService pool = new ThreadPoolExecutor(
                2,          // 核心线程数，一直存在的线程数量
                5,      // 最大扩容线程数，在等待队列满了之后才进行扩容，并不是核心线程满了才扩容。注意：新扩容的执行线程并不执行在等待队列的任务，而是执行新进来的，相当于谁导致的扩容谁先执行
                100L,      // 扩容线程最大空闲时间，在指定时间内如果没有新的任务执行的话，则进行回收，缩容
                TimeUnit.SECONDS,       // 扩容线程最大空闲时间单位
                new LinkedBlockingDeque<>(3),   // 阻塞队列，用于存放等待的执行的线程，默认阻塞队列大小是  MAX_INTEGER
                Executors.defaultThreadFactory(),       //
//                new ThreadPoolExecutor.AbortPolicy()
//                new ThreadPoolExecutor.CallerRunsPolicy()
//                new ThreadPoolExecutor.DiscardOldestPolicy()
//                new ThreadPoolExecutor.DiscardPolicy()
                new MyRejectedPolicy()                  //
        );

        try {
            for (int i = 1; i <= 17; i++) {
                TimeUnit.MILLISECONDS.sleep(100);
                final int tmpint = i;
                pool.execute(new Runnable() {
                    @SneakyThrows
                    @Override
                    public void run() {
                        System.out.println(Thread.currentThread().getName() + "~~" + tmpint + "~~~" + ThreadLocalUtils.get());
                        TimeUnit.SECONDS.sleep(4);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            pool.shutdown();
        }
        System.out.println("====================");
    }

}
