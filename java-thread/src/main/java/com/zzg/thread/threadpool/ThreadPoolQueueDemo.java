package com.zzg.thread.threadpool;

import java.util.concurrent.*;

/**
 * @Author 张志刚
 * @Date 2022/1/5
 * @Description TODO
 * 线程池使用不同的队列下的情况
 * <p>
 * https://www.cnblogs.com/dafanjoy/p/9729358.html
 */
public class ThreadPoolQueueDemo {

    private static ExecutorService pool = new ThreadPoolExecutor(
            1,
            5,
            100L,
            TimeUnit.SECONDS,
            /**
             * 《直接提交队列》
             * 设置为SynchronousQueue队列，SynchronousQueue是一个特殊的BlockingQueue，
             * 它没有容量，每执行一个插入操作就会阻塞，需要再执行一个删除操作才会被唤醒，反之每一个删除操作也都要等待对应的插入操作。
             * 当正在执行的线程数超过最大线程数， 直接执行拒绝策略，但是线程不会被抛弃，所有线程都将被执行完毕
             * 不是很明白应用场景
             */
//            new SynchronousQueue<>(),

            /**
             * 《有界的任务队列》
             * 可以设置队列长度的有界队列，用来控制等待的数据，避免太多的线程在等待执行导致性能不足，OOM等
             */
//            new ArrayBlockingQueue<>(3),

            /**
             * 阻塞队列，用于存放等待的执行的线程，默认阻塞队列大小是  MAX_INTEGER
             * 不明白跟ArrayBlockingQueue的区别，可能就是LinkedList 和 ArrayList的区别
             */
            new LinkedBlockingDeque<>(300),

            /**
             * 需要任务线程实现 Comparable接口，用于指定哪些任务优先执行，
             * 所有任务并不是按照先进先出的队列进行执行，而是根据具体的实现方法操作
             * 参考上面文章
             */
//            new PriorityBlockingQueue<>(),

            Executors.defaultThreadFactory(),
            new MyRejectedPolicy()
    );

    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            final int tmpInt = i;
            pool.execute(() -> {
                System.out.println("Thread-Name-" + tmpInt + "; " + Thread.currentThread().getName() + "; time = " + System.currentTimeMillis());
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }

}
