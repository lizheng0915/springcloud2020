package com.zzg.thread.threadpool;

import com.zzg.springcloud.common.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author 张志刚
 * @Date 2021/12/28
 * @Description 使用线程池的多线程修改外部数据
 */
public class ThreadPoolDemo4 {

    // 虽然不建议使用此方法创建线程池，但是测试代码，简洁最好
    private ExecutorService fixedPool = Executors.newFixedThreadPool(30);
    private ExecutorService fixedPool2 = Executors.newFixedThreadPool(30);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ThreadPoolDemo4 demo4 = new ThreadPoolDemo4();
//        int value = demo4.getTotalValue(100);
        int value = demo4.getTotalValue2(100);      // 线程
        System.out.println(value);
    }

    /**
     * @param cook
     * @return 通过判断Future来确定执行完毕
     */
    private int getTotalValue(int cook) throws ExecutionException, InterruptedException {
        List<Future> futures = new ArrayList<>();
        AtomicInteger totalValue = new AtomicInteger();
        for (int i = 0; i < cook; i++) {
            Future future = fixedPool.submit(() -> {
                int rand = RandomUtils.getRandom(cook);
                int temp = totalValue.addAndGet(rand);
                System.out.println("rand = " + rand + "; temp = " + temp);
            });
            futures.add(future);
        }
        for (Future f : futures) {
            f.get();
        }
        System.out.println("totalValue -> " + totalValue);
        return totalValue.get();
    }

    /**
     * @param cook
     * @return 通过CountDownLatch来判断线程执行完毕
     * 线程中创建子线程，同时都要使用 CountDownLatch 进行结束判断的时候，容易产生死锁 - 当线程池核心线程数不足时容易死锁
     * 当前例子中： 核心线程数是30，要执行的任务数是 100就容易死锁，当执行的任务数是10的时候就不容易死锁
     */
    private int getTotalValue2(int cook) throws InterruptedException {
        CountDownLatch downLatch = new CountDownLatch(cook);
        AtomicInteger totalValue = new AtomicInteger();
        for (int i = 0; i < cook; i++) {
            final int index = i;
            fixedPool2.submit(() -> {
                int rand = RandomUtils.getRandom(cook);
                int temp = totalValue.addAndGet(rand);
                System.out.println("loop 1 : rand = " + rand + "; temp = " + temp + "; index = " + index);

                CountDownLatch downLatchInner = new CountDownLatch(20);
                for (int jj = 0; jj < 20; jj++) {
                    fixedPool2.submit(() -> {
                        int rand2 = RandomUtils.getRandom(cook);
                        int temp2 = totalValue.addAndGet(rand2);
                        System.out.println("loop 2 : rand2 = " + rand2 + "; temp2 = " + temp2 + "; index = " + index);
                        downLatchInner.countDown();
                    });
                }
                /*try {
                    downLatchInner.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                downLatch.countDown();
            });
        }
        downLatch.await();
        System.out.println("totalValue -> " + totalValue);
        return totalValue.get();
    }
}
