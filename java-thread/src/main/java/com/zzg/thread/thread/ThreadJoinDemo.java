package com.zzg.thread.thread;

import com.zzg.springcloud.common.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadJoinDemo extends Thread {

    private AtomicInteger atomicInteger;

    public ThreadJoinDemo() {
    }

    public ThreadJoinDemo(AtomicInteger atomicInteger) {
        this.atomicInteger = atomicInteger;
    }


    @Override
    public void run() {
        int rand = RandomUtils.getRandom(2000);
        this.atomicInteger.addAndGet(rand);
        System.out.println(Thread.currentThread().getName() + "~~~come in thread demo run method~~~ rand = " + rand);
        try {
            Thread.sleep(rand);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws InterruptedException {
//        joinTest1();
//        joinTest1_1();
        joinTest2();
    }

    /**
     * join() - 表示后面线程要等待当前线程执行完毕后才能执行 - 让其他线程等待，即便当前线程是阻塞状态
     * joinTest1 - 测试线程的 join 方法，
     * 按此写法，其实效果就是线程串行执行了，第二个线程要等第一个执行完毕后才能开始执行，以此类推
     * 总耗时相当于多个线程单独执行时间之和
     */
    public static void joinTest1() throws InterruptedException {
        long start = System.currentTimeMillis();
        AtomicInteger atomicCount = new AtomicInteger();

        ThreadJoinDemo t1 = new ThreadJoinDemo(atomicCount);
        ThreadJoinDemo t2 = new ThreadJoinDemo(atomicCount);
        ThreadJoinDemo t3 = new ThreadJoinDemo(atomicCount);

        t1.start();
        t1.join();
        t2.start();
        t2.join();
        t3.start();
        t3.join();

        long end = System.currentTimeMillis();
        System.out.println("totalCost = " + (end - start));
        System.out.println("atomicCount = " + atomicCount.get());
    }

    public static void joinTest1_1() throws InterruptedException {
        long start = System.currentTimeMillis();
        AtomicInteger atomicCount = new AtomicInteger();

        for (int i = 0; i < 3; i++) {
            ThreadJoinDemo t1 = new ThreadJoinDemo(atomicCount);
            t1.start();
            t1.join();
        }

        long end = System.currentTimeMillis();
        System.out.println("totalCost = " + (end - start));
        System.out.println("atomicCount = " + atomicCount.get());
    }


    /**
     * join() - 表示后面线程要等待当前线程执行完毕后才能执行 - 让其他线程等待，即便当前线程是阻塞状态
     * joinTest1 - 测试线程的 join 方法，
     * 按此写法，相当于所有线程都已经开始执行，然后等待所有线程执行完毕后才执行后面的主线程
     * 总耗时相当于执行时间最长的线程。
     */
    public static void joinTest2() throws InterruptedException {
        long start = System.currentTimeMillis();
        AtomicInteger atomicCount = new AtomicInteger();

        ThreadJoinDemo t1 = new ThreadJoinDemo(atomicCount);
        ThreadJoinDemo t2 = new ThreadJoinDemo(atomicCount);
        ThreadJoinDemo t3 = new ThreadJoinDemo(atomicCount);

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        long end = System.currentTimeMillis();
        System.out.println("totalCost = " + (end - start));
        System.out.println("atomicCount = " + atomicCount.get());
    }

    public static void joinTest2_1() throws InterruptedException {
        long start = System.currentTimeMillis();
        AtomicInteger atomicCount = new AtomicInteger();

        List<ThreadJoinDemo> threadList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            ThreadJoinDemo thread = new ThreadJoinDemo(atomicCount);
            thread.start();
            threadList.add(thread);
        }

        for (ThreadJoinDemo thread : threadList) {
            thread.join();
        }

        long end = System.currentTimeMillis();
        System.out.println("totalCost = " + (end - start));
        System.out.println("atomicCount = " + atomicCount.get());
    }

}
