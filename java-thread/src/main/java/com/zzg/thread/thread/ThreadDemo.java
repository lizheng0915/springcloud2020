package com.zzg.thread.thread;

/**
 * 创建线程对象设置字段属性
 * 可以通过构造方法或者set方法来传递数据
 */
public class ThreadDemo extends Thread {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ThreadDemo() {
    }

    public ThreadDemo(String status) {
        this.status = status;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "~~~come in thread demo run method~~~status = " + getStatus());
    }

    public static void main(String[] args) {
        ThreadDemo t1 = new ThreadDemo();
        t1.setStatus("345");
        t1.start();

        ThreadDemo t2 = new ThreadDemo("123");
        t2.start();
    }
}
