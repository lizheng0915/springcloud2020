package com.zzg.thread.thread;

import com.zzg.springcloud.common.util.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SimpleDateFormat 对象线程不安全证明
 * 解决办法:
 * 1. 将SimpleDateFormat定义成局部变量，各个线程和方法自行创建对象
 * 2. 使用同步代码块将
 * 3. 使用ThreadLocal，每个线程都拥有自己的SimpleDateFormat对象副本。
 * 4. 使用DateTimeFormatter代替SimpleDateFormat
 * <p>
 * SimpleDateFormat线程不安全原因及解决方案
 * https://www.cnblogs.com/yangyongjie/p/11017409.html
 */
public class SimpleDateFormatDemo {

    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
//        exampleMultitle();
        exampleMultitleSuccess();
    }

    /**
     * 证明，在调用工具方法的时候，多线程也是安全的
     * 在工具方法中，每次新建 SimpleDateFormat 对象
     */
    public static void exampleMultitleSuccess() {
        for (int i = 1; i <= 100; i++) {
            new Thread(() -> {
                try {
                    String str = DateUtils.getDatetime();
                    Date parseDate = DateUtils.getDateByDatetimeStr(str);
                    String str2 = DateUtils.getDatetime(parseDate);
                    System.out.println(str.equals(str2));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, "Thread-Name-" + i).start();
        }
    }

    /**
     * 证明在多线程下 SimpleDateFormat 不是线程安全的
     * 打印结果有 false，并且还会报异常
     */
    public static void exampleMultitle() {
        for (int i = 1; i <= 100; i++) {
            new Thread(() -> {
                String str = sdf.format(new Date());
                try {
                    Date parseDate = sdf.parse(str);
                    String str2 = sdf.format(parseDate);
                    System.out.println(str.equals(str2));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, "Thread-Name-" + i).start();
        }
    }

    /**
     * 证明单线程下 SimpleDateFormat 是线程安全的
     * 打印的所有都是true
     */
    public static void exampleSingle() {
        for (int i = 1; i <= 1000; i++) {
            String str = sdf.format(new Date());
            try {
                Date parseDate = sdf.parse(str);
                String str2 = sdf.format(parseDate);
                System.out.println(str.equals(str2));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
