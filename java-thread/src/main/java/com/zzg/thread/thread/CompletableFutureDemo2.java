package com.zzg.thread.thread;

import com.zzg.springcloud.common.util.PrintUtils;
import com.zzg.springcloud.common.util.SleepUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;


/**
 * 不使用线程池的情况下
 * CompletableFuture 最多使用的线程数是怎么来的呢？ 跟当前机器的CPU核心数和逻辑CPU个数有关
 * Runtime.getRuntime().availableProcessors() 就是当前系统逻辑CPU个数 - 1
 * ForkJoinPool.getCommonPoolParallelism() = Runtime.getRuntime().availableProcessors() - 1
 */
public class CompletableFutureDemo2 {

    public static void main(String[] args) {
        // 查看当前系统逻辑CPU个数
        PrintUtils.printThreadAndMessage(Runtime.getRuntime().availableProcessors());

        // 查看 当前线程数
        PrintUtils.printThreadAndMessage(ForkJoinPool.commonPool().getPoolSize());

        // 查看 最大线程数
        PrintUtils.printThreadAndMessage(ForkJoinPool.getCommonPoolParallelism());

        findCoreThreadNum();
    }

    /**
     * 找出 CompletableFuture 的核心线程数
     */
    public static void findCoreThreadNum() {
        /**
         * 设置默认的最大线程数 - 但是一般情况下我们不去修改这个数字，因为这个并不一定只是CompletableFuture使用
         * JVM方式设置 + 系统启动时设置
         */
        //JVM启动参数设置最大线程数  -Djava.util.concurrent.ForkJoinPool.common.parallelism=8
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "20");

        long start = System.currentTimeMillis();
        List<CompletableFuture> futureList = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            final int tempInt = i;
            CompletableFuture future = CompletableFuture.runAsync(() -> {
                SleepUtils.sleepMilliseconds(1000);
                PrintUtils.printThreadAndMessage("Thread-" + tempInt + " 处理完成。" + System.currentTimeMillis());
            });
            futureList.add(future);
        }
        // 等待所有线程执行完毕
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()])).join();
        PrintUtils.printThreadAndMessage("所有任务已经处理完成，总耗时 : " + (System.currentTimeMillis() - start));
    }

}
