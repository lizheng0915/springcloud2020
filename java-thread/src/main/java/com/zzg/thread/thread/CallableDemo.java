package com.zzg.thread.thread;


import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 通过实现 Callable 接口来创建一个带返回值的线程
 */

public class CallableDemo implements Callable<String> {

    /**
     * Callable 接口必须实现  call 方法
     * 返回值等同于集成Callable时确定的
     */
    @Override
    public String call() {
        System.out.println(Thread.currentThread().getName() + "~~~come in call method~~~");
        return "1024";
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /**
         * 定义一个线程，如何让其执行 CallableDemo 线程里面的 call方法
         * 1. Thread 的构造方法没有让传入 Callable 对象的方法，并且 Callable 跟 Runnable 接口也没有任何关联
         * 2. 通过适配器模式寻找替代方案
         *
         * FutureTask可用于包装Callable或Runnable对象。 因为FutureTask实现Runnable ，一个FutureTask可以提交到一个Executor执行。
         * Runnable --> RunnableFuture --> FutureTask 所以往 Thread 里面传入一个 FutureTask 是可以的
         * FutureTask 的构造方法
         * FutureTask(Callable<V> callable)
         * FutureTask(Runnable runnable, V result)
         * 所以用 Callable 接口创造一个 FutureTask 类
         */
        FutureTask<String> futureTask = new FutureTask<>(new CallableDemo());
        Thread t1 = new Thread(futureTask, "Thread-Name-AAA");
        System.out.println("t1.getName() = " + t1.getName());
        System.out.println("t1.getId() = " + t1.getId());
        System.out.println("t1.getState() = " + t1.getState());
        t1.start();

        /**
         * 通过 FutureTask get() 方法的阻塞特性，获取返回值
         */
        String res = futureTask.get();
        System.out.println("futureTask.get() = " + res);
    }

}
