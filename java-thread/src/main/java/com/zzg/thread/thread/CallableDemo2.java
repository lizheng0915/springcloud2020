package com.zzg.thread.thread;

import com.zzg.springcloud.common.util.PrintUtils;
import com.zzg.springcloud.common.util.RandomUtils;
import com.zzg.springcloud.common.util.SleepUtils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 实现线程的方法之一 : 实现 Callable 接口
 * 1. Callable 是有返回值的多线程实现接口
 * 2. Callable 是一个函数式接口，可以直接使用函数式接口实现
 */
public class CallableDemo2 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        /**
         * 第一种方法: 创建 callable 接口对象实现线程
         */
        PrintUtils.printThreadAndMessage("1. 我是main线程 - 开始。");
        Callable<String> callable = () -> {
            PrintUtils.printThreadAndMessage("2. 我是callable创建的子线程 - 开始。");
            SleepUtils.sleepMilliseconds(100, 200);     // 子线程创建后睡眠100-200ms
            PrintUtils.printThreadAndMessage("3. 我是callable创建的子线程 - 结束");
            return "callable 接口返回内容。 Random = " + RandomUtils.getRandom(1000);
        };

        /**
         * 通过Future接口或者FutureTask实现类定义任务对象
         * 由于 Thread 对象的构造方法只支持 Runnable 接口的实现，所以必须使用 FutureTask 对象， 使用Future接口不行
         */
        FutureTask<String> futureTask = new FutureTask<>(callable);
        Thread thread = new Thread(futureTask);
        thread.start();
        String res = futureTask.get();
        PrintUtils.printThreadAndMessage("4. 我是main线程 - 第一部分完结。撒花~~~callable res = " + res);

        System.out.println("---------------------------------------------------------------");
        /**
         * 第二种方法: 直接在thread构造函数中实现接口
         */
        PrintUtils.printThreadAndMessage("2.1 我是main线程 - 我又开始。");
        FutureTask<String> futureTask1 = new FutureTask<>(() -> {
            PrintUtils.printThreadAndMessage("2.2 我是callable创建的子线程 - 开始。");
            SleepUtils.sleepMilliseconds(100, 200);     // 子线程创建后睡眠100-200ms
            PrintUtils.printThreadAndMessage("2.3 我是callable创建的子线程 - 结束");
            return "callable 接口返回内容。 Random = " + RandomUtils.getRandom(1000);
        });
        Thread thread1 = new Thread(futureTask1);
        thread1.start();
        String res1 = futureTask1.get();
        PrintUtils.printThreadAndMessage("2.4 我是main线程 - 第二部分完结。撒花~~~ callable res1 = " + res1);
    }

}
