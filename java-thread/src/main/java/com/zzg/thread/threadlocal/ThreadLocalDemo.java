package com.zzg.thread.threadlocal;

import java.util.UUID;

/**
 * @author 张志刚
 * 证明 : 不同线程，即便修改、获取的是同一个ThreadLocal对象的值，数据隔离
 */
public class ThreadLocalDemo {

    static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void main(String[] args) {
        ThreadLocalDemo2 demo2 = new ThreadLocalDemo2();
        System.out.println("threadLocal = " + threadLocal);

        threadLocal.set(Thread.currentThread().getName());
        System.out.println("main --> threadLocal.get() = " + threadLocal.get());
        threadLocal.remove();
        threadLocal.set(UUID.randomUUID().toString());

        new Thread(() -> {
            System.out.println("AA --> threadLocal = " + threadLocal);
            threadLocal.set(Thread.currentThread().getName());
            System.out.println("AA --> threadLocal.get() = " + threadLocal.get());
            threadLocal.remove();
            System.out.println("AAA --> threadLocal.get() = " + threadLocal.get());
        }, "Thread-Name-AA").start();

        new Thread(() -> {
            System.out.println("BB --> threadLocal = " + threadLocal);
            threadLocal.set(Thread.currentThread().getName());
            System.out.println("BB --> threadLocal.get() = " + threadLocal.get());
            threadLocal.remove();
            System.out.println("BBB --> threadLocal.get() = " + threadLocal.get());
        }, "Thread-Name-BB").start();

        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println("main - main --> threadLocal.get() = " + threadLocal.get());

        System.out.println("main3 - main3 --> threadLocal.get() = " + threadLocal.get());
    }

}
