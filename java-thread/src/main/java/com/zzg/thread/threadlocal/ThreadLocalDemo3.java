package com.zzg.thread.threadlocal;

public class ThreadLocalDemo3 {

    public static void m1() {
        System.out.println("========ThreadLocalDemo3==m1====");
        System.out.println("threadName = " + Thread.currentThread().getName() + "; traceId = " + ThreadLocalUtils.get());
    }

    public void m2() {
        System.out.println("========ThreadLocalDemo3==m2====");
        System.out.println("threadName = " + Thread.currentThread().getName() + "; traceId = " + ThreadLocalUtils.get());
    }
}
