package com.zzg.thread.threadlocal;

/**
 * @author 张志刚
 * 证明 : 在使用中间类 ThreadLocalUtils 的时候，也能保证不同线程之间的数据安全
 */
public class ThreadLocalDemo4 {

    public static void main(String[] args) {
        System.out.println("main --> ThreadLocalUtils.get() = " + ThreadLocalUtils.get());
        ThreadLocalDemo2 demo2 = new ThreadLocalDemo2();
        demo2.m2();

        new Thread(() -> {
            System.out.println("AA --> ThreadLocalUtils.get() = " + ThreadLocalUtils.get());
            demo2.m2();
        }, "AA").start();

        new Thread(() -> {
            System.out.println("BB --> ThreadLocalUtils.get() = " + ThreadLocalUtils.get());
            demo2.m2();
        }, "BB").start();

    }

}
