package com.zzg.springcloud.order.feignclient;

import com.zzg.springcloud.common.result.ReturnResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * 使用Feign直接调用没有注册到当前集群的HTTP请求
 *
 * @FeignClient 注解注意事项
 * 1. value 或者 name 是必填的，但是并没有注册到 Eureka Server, 我们可以随便编写一个服务名称
 * 2. url 填写请求地址的前缀 - ip + port
 */
@Component
@FeignClient(value = "A-B", url = "http://118.190.209.120:8080")
public interface AutoplayClient {

    @GetMapping(value = "/autoplay/tongji/runstatus")
    ReturnResult runstatus(@RequestParam("date") String date, @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize);

}
