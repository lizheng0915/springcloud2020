package com.zzg.springcloud.order.config;


import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 默认的 Feign Config
 */

@Configuration
public class DefaultFeignConfig {

    // 配置日志级别
    // NONE --> BASIC --> HEADERS --> FULL
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.BASIC;
    }

}
