package com.zzg.logging.JCL;

/*import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;*/

/**
 * <dependency>
 * <groupId>commons-logging</groupId>
 * <artifactId>commons-logging</artifactId>
 * <version>1.2</version>
 * </dependency>
 * jcl = jakarta commons-logging.jar
 * 最新版本是 1.2 最新的更新时间是 2014年，看来会随着时间的推移慢慢也会推出历史舞台
 * <p>
 * 可以自动在 jul 和 log4j 之间进行切换，在 LogFactoryImpl 中默认指定了4中实现方法，
 * 源码：查看顺序
 * org.apache.commons.logging.LogFactory#getLog(java.lang.Class) -->
 * org.apache.commons.logging.impl.LogFactoryImpl#getInstance(java.lang.Class)-->
 * org.apache.commons.logging.impl.LogFactoryImpl#getInstance(java.lang.String)-->
 * org.apache.commons.logging.impl.LogFactoryImpl#newInstance(java.lang.String)-->
 * org.apache.commons.logging.impl.LogFactoryImpl#discoverLogImplementation(java.lang.String)-->
 * org.apache.commons.logging.impl.LogFactoryImpl#classesToDiscover
 * <p>
 * 1. 如果没有引入 log4j，那么默认使用 java.util.logger 的日志体系
 * 2. 如果引入了 log4j 的jar包，那么默认使用 log4j 的日志体系
 * 3. jcl对不同的日志进行了适配，使用的是适配器的设计模式，是在运行时通过类加载器机制动态的获取了 log 的具体实现
 * jcl本身并不进行任何的日志打印，都是调用  jul 或者 log4j 的实现进行日志打印
 */

/*
public class CommonLoggingDemo {

    private static final Log log = LogFactory.getLog(CommonLoggingDemo.class);

    public static void main(String[] args) {
        log.trace("This is TRACE message---》跟踪日志");
        log.debug("This is DEBUG message---》断点日志");
        log.info("This is INFO message---》普通日志");
        log.warn("This is WARN message---》警告日志");
        log.error("This is ERROR message---》错误日志");
        log.error("This is Exception", new Exception("customException"));
    }

}
*/


