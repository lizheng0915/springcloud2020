package com.zzg.logging.log4j;


/*
import com.wjj.log4j.Wjj_Log4j_1_2_17;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
*/


/**
 * <dependency>
 * <groupId>log4j</groupId>
 * <artifactId>log4j</artifactId>
 * <version>1.2.17</version>
 * </dependency>
 * <p>
 * 最新版本是 1.2.17 , 在2012年之后就不再维护
 * <p>
 * log4j 1.x 版本
 * 1. 引入jar包
 * 2. 编写配置文件 - log4j.properties - 配置文件名字固定 也可以是  log4j.xml 两种写法
 */
/*public class Log4j_1_2_17 {

    private final static Logger logger = Logger.getLogger(Log4j_1_2_17.class);

    public static void main(String[] args) {
        for (int i = 0; i < 1; i++) {
            logger.trace("This is TRACE message---》跟踪日志");
            logger.debug("This is DEBUG message---》断点日志");
            logger.info("This is INFO message---》普通日志");
            logger.warn("This is WARN message---》警告日志");
            logger.error("This is ERROR message---》错误日志");
            logger.error("This is Exception", new Exception("customException"));
        }
        Wjj_Log4j_1_2_17.good();

        List<String> list = new ArrayList<>();
        try {
            System.out.println(list.get(2));
        } catch (Exception e) {
            logger.error("获取数据异常", e);
        }
    }

}*/
