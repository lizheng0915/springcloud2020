/*
package com.zzg.logging.slf4j.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// slf4j + logback 的日志系统
public class LogBackDemo {

    private static final Logger logger = LoggerFactory.getLogger(LogBackDemo.class);

    private static final Logger logger2 = LoggerFactory.getLogger("com.zzg.logback");

    private static final Logger logger3 = LoggerFactory.getLogger("org.apache");

    public static void main(String[] args) throws InterruptedException {

        String str1 = "";
        String str2 = "";
        for (int i = 0; i < 2; i++) {
//            str1 += "12345678901234567890123456789012345678901234567890";
//            str2 += "中华人民共和国万岁的中华人民共和国万岁的中华人民共和国万岁的中华人民共和国万岁的中华人民共和国万岁的";
            // slf4j 日志级别  TRACE < DEBUG < INFO < WAIN < ERROR
            logger.trace("可以使用变量 : trace = {}, index = {}", "THIS IS trace", i);
            logger.debug("可以使用变量 : debug = {}, index = {}", "THIS IS debug", i);
            logger.info("可以使用变量  : info  = THIS IS info, No argument");
            logger.warn("可以使用变量  : warn  = {}, index = {}", "THIS IS warn", i);
            logger.error("可以使用变量 : error = {}, index = {}, str1 = {}, length1 = {}", "THIS IS error1", i, str1, str1.length());
//            logger.error("可以使用变量 : error = {}, index = {}, str2 = {}, length2 = {}", "THIS IS error2", i, str2, str2.length());
            Thread.sleep(5000);
//            logger.error("DBAppender : {}", new DBAppender());

//            logger2.trace("可以使用变量 : trace = {}, index = {}", "THIS IS trace", i);
//            logger2.debug("可以使用变量 : debug = {}, index = {}", "THIS IS debug", i);
//            logger2.info("可以使用变量  : info  = THIS IS info, No argument");
//            logger2.warn("可以使用变量  : warn  = {}, index = {}", "THIS IS warn", i);
//            logger2.error("可以使用变量 : error = {}, index = {}, str1 = {}, length1 = {}", "THIS IS error1", i, str1, str1.length());
//
//            Thread.sleep(5000);

//            logger3.trace("可以使用变量 : trace = {}, index = {}", "THIS IS trace", i);
//            logger3.debug("可以使用变量 : debug = {}, index = {}", "THIS IS debug", i);
//            logger3.info("可以使用变量  : info  = THIS IS info, No argument");
//            logger3.warn("可以使用变量  : warn  = {}, index = {}", "THIS IS warn", i);
//            logger3.error("可以使用变量 : error = {}, index = {}, str1 = {}, length1 = {}", "THIS IS error1", i, str1, str1.length());


        }

    }

}
*/
