package com.zzg.logging.slf4j.logback.filter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;


/**
 * 1. 实现一个简单的过滤器
 * 跟指定的level匹配的时候输出日志，不相符的时候不输出日志
 * 说明: ch.qos.logback.classic.filter.LevelFilter匹配时返回的是 onMatch 的值，不匹配时返回的是 onMismatch 的值，是自由配置的
 * 本例子的是直接返回结果
 */
public class MyLevelFilter extends Filter<ILoggingEvent> {

    /**
     * 定义属性和set方法，可以从配置文件中获取指定的日志等级
     */
    private Level level;
    public void setLevel(Level level) {
        this.level = level;
    }

    @Override
    public FilterReply decide(ILoggingEvent event) {
        if (!isStarted()) {
            return FilterReply.NEUTRAL;
        }

        if (event.getLevel().equals(level)) {
            return FilterReply.ACCEPT;
        }

        return FilterReply.DENY;
    }
}
