package com.zzg.webapp.beaninit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @Author 张志刚
 * @Date 2022/3/9
 * @Description TODO
 * <p>
 * https://blog.csdn.net/weixin_42608550/article/details/97675350
 */
public class SpringBeanInitProcess {

    /**
     * bean 初始化之后调用的方法，使用 PostConstruct 修饰
     */
    @PostConstruct
    public void init() {
        System.out.println("======after bean create======PostConstruct======");
    }

    /**
     * bean 销毁之前调用的方法，使用 PreDestroy 修饰
     */
    @PreDestroy
    public void destroy() {
        System.out.println("======before bean destroy======PreDestroy======");
    }

}
