package com.zzg.webapp;


import com.zzg.springcloud.common.util.DateUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootWebApplicationStart {

    public static void main(String[] args) {
        SpringApplication.run(BootWebApplicationStart.class, args);
        System.out.println(DateUtils.getDatetime() + "\t======BootWebApplicationStart    启动成功======");
    }

}
