package com.zzg.lambda.foreach;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author 张志刚
 * @Date 2021/12/28
 * @Description TODO
 */
public class LambdaForEachDemo {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add(i);
        }
        for (Integer i : list) {
            System.out.println(i);
        }
        AtomicInteger sum = new AtomicInteger(0);
        StringBuffer sb = new StringBuffer();
        StringBuilder sbu = new StringBuilder();
        System.out.println("======================start=================");
        list.forEach(data -> {
            sum.addAndGet(data);
            sb.append(data + "-");
            sbu.append(data + ",");
            System.out.println(data);
        });
        System.out.println("======================end=================");
        System.out.println("sum = " + sum.get());
        System.out.println(sb);
        System.out.println(sbu);

        int max = list.stream().mapToInt(e -> e).max().getAsInt();
        System.out.println(max);
        list.parallelStream().forEach((s) -> {
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("lambda表达式的线程;;;" + s);
        });
        System.out.println("================lambda表达式的线程======end=================");
    }


}
