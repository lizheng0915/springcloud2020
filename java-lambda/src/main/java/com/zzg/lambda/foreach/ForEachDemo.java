package com.zzg.lambda.foreach;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.IntStream;

/**
 * @Author 张志刚
 * @Date 2022/1/13
 * @Description TODO
 */
public class ForEachDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        IntStream.rangeClosed(1, 10).forEach(i -> {
            list.add(String.valueOf(i));
        });
        System.out.println(list);
        StringJoiner joiner = new StringJoiner("#", "(", ")");
        list.forEach(item -> {
            joiner.add(item);
        });
        System.out.println(joiner.toString());

    }

}
