
## JVM启动参数修改

- Xms : 设置JVM启动时分配的堆内存大小
- Xmx : 设置JVM运行时可以向系统申请的最大堆内存大小
- Xss : 设置JVM启动时为每一个线程分配的内存大小，默认JDK1.4是256K，JDK1.5+是1M

## Linux 服务器下修改 Tomcat的启动文件 - bin/catalina.sh 文件
```
JAVA_OPTS="-server -Xms2048M -Xmx2048M -Xss512K -XX:PermSize=256m -XX:MaxPermSize=512m -XX:MaxNewSize=512m"
```

```
JAVA_OPTS="-Xmx2048m -XX:MetaspaceSize=512m -XX:MaxMetaspaceSize=768m -Xss2m"
CATALINA_OPTS="-Djava.awt.headless=true"
```

## Tomcat 启动警告日志
```
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option PermSize=256m; support was removed in 8.0
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=512m; support was removed in 8.0
```
Tomcat 在启动时，报上面两句警告内容，说明两个参数已经在JAVA8中不支持了。
### 原因
JDK8中，MaxPermSize已不再支持。将 XX:MaxPermSize 修改为 XX:MaxMetaspaceSize



