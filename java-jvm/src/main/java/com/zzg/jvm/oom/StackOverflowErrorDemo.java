package com.zzg.jvm.oom;


/**
 * java.lang.StackOverflowError
 * 栈空间溢出
 * 方法的深度调用，比如递归调用，容易引起 StackOverflowError
 * 方法自己调自己的无线循环，必然引发 StackOverflowError
 */
public class StackOverflowErrorDemo {

    public static void main(String[] args) {
        cycleMethodInvoke();
    }

    /**
     * 方法循环调用自己
     * Exception in thread "main" java.lang.StackOverflowError
     */
    private static void cycleMethodInvoke() {
        cycleMethodInvoke();
    }

}
