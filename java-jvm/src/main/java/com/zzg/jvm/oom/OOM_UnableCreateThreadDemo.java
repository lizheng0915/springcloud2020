package com.zzg.jvm.oom;

import java.util.concurrent.TimeUnit;

/**
 * java.lang.OutOfMemoryError: unable to create new native thread
 * 不能再创建更多本地线程异常。
 * 超过线程可以创建进程的数量限制，这个跟具体的语言和程序没有关系，主要是跟操作系统相关，
 * Linux默认一个进程可以创建1024个线程
 * Window下这个代码跑了7W多个线程仍然没有抛出异常，但是电脑已经卡的不行不行的了，不敢再window下实验了。
 */
public class OOM_UnableCreateThreadDemo {

    public static void main(String[] args) {
        int i = 1;
        try {
            while (true) {
                System.out.println("i = " + i++);
                new Thread(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(Integer.MAX_VALUE);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }, "" + i).start();
            }
        } finally {
            System.out.println("i = " + i);
        }

    }

}
