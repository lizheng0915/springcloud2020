package com.zzg.jvm.oom;

import java.util.ArrayList;
import java.util.List;

/**
 * java.lang.OutOfMemoryError: GC overhead limit exceeded
 * 当程序写的有问题，GC在一直回收垃圾，GC回收时间过长。
 * GC时间过长的定义是：超过98%的时间用来做GC垃圾回收，并且收回的内存微乎其微，很快内存又被占满导致继续GC，不停的GC导致程序无法正常运行，才会抛出此异常。
 * 此时堆内存并没有完全被占满，只是JVM感觉不能再这样做无用功了。
 */
public class OOM_GCOverheadLimitDemo {
    /**
     * -XX:MaxDirectMemorySize 堆外直接内存最大使用量
     * -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:MaxDirectMemorySize=5M
     */
    public static void main(String[] args) {
        int i = 0;
        List<String> list = new ArrayList<>();
        try {
            while (true) {
                list.add(String.valueOf(i++).intern());
            }
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        } finally {
            System.out.println("i = " + i);
            System.out.println("list.size() = " + list.size());
        }
    }

}
