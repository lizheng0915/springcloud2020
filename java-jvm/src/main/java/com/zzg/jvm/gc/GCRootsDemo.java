package com.zzg.jvm.gc;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * GC Roots : JAVA中可以作为GC Roots的对象
 * 虚拟机栈中引用的对象 - 方法区就是在栈中，方法区中还在引用的对象
 * 方法区中的类的静态属性引用的对象
 * 方法区中的常量引用的对象
 * 本地方法栈中JNI（一般说的Native方法）引用的对象
 */
public class GCRootsDemo {


    // 方法区中的类的静态属性引用的对象 - list 可作为GC Root
    private static List<String> list = new ArrayList<>();

    // 方法区中的常量引用的对象 - GC_MAP 可作为GC Root
    private static final Map<String, Object> GC_MAP = new ConcurrentHashMap<>();


}
