package com.zzg.jvm.gc;

import java.util.concurrent.TimeUnit;

/**
 * JVM 参数讲解demo
 * <p>
 * 1. 如何查看一个正常运行中的程序，它的某一个JVM参数是否开启？
 * 比如：PrintGCDetails - 查看是否打印GC收集细节的参数配置
 * jps -l   // 查看得到当前运行的进程ID
 * jinfo -flag {paramsName} {PID}
 * jinfo -flag PrintGCDetails 48316
 * 得到结果是  -XX:-PrintGCDetails  // 由于是 -XX:- 表示没有开启此参数
 * 2. 如何查看一个正常运行中的程序，它的某一个JVM参数具体的值是多少?
 * 比如: MetaspaceSize - 元空间内存大小
 * jps -l   // 查看得到当前运行的进程ID
 * jinfo -flag MetaspaceSize 48316
 * 得到结果是  -XX:MetaspaceSize=21807104  // 看得出，默认值是21M左右。 KV键值对的参数类型，没有是否开启一说，是必须要开启的，只是要设置不同的值
 */
public class JVM_ParamsDemo {

    public static void main(String[] args) throws InterruptedException {

        System.out.println(System.getProperty("java.vm.name"));
        long toMB = 1024 * 1024L;

        /**
         * -Xmx10M -Xms10M
         * availableProcessors - 当前机器CPU核数
         * totalMemory - JAVA虚拟机的内存总量（字节） - 初始堆内存（-Xms） - 默认本机物力内存的  1/64，相当于程序当前已使用内存 + freeMemory 之和
         * maxMemory - JAVA虚拟机试图使用的最大内存总量（字节） - 最大堆内存（-Xmx） - 默认本机物力内存的  1/4
         * freeMemory - 空闲内存大小
         */
        System.out.println("Runtime.getRuntime().availableProcessors() = " + Runtime.getRuntime().availableProcessors());
        System.out.println("Runtime.getRuntime().totalMemory() = " + Runtime.getRuntime().totalMemory() / toMB);
        System.out.println("Runtime.getRuntime().maxMemory() = " + Runtime.getRuntime().maxMemory() / toMB);
        System.out.println("Runtime.getRuntime().freeMemory() = " + Runtime.getRuntime().freeMemory() / toMB);

        TimeUnit.SECONDS.sleep(2);
        System.out.println("==========================");
        byte[] bytes = new byte[7 * 1024 * 1024];

        System.out.println("Runtime.getRuntime().availableProcessors() = " + Runtime.getRuntime().availableProcessors());
        System.out.println("Runtime.getRuntime().totalMemory() = " + Runtime.getRuntime().totalMemory() / toMB);
        System.out.println("Runtime.getRuntime().maxMemory() = " + Runtime.getRuntime().maxMemory() / toMB);
        System.out.println("Runtime.getRuntime().freeMemory() = " + Runtime.getRuntime().freeMemory() / toMB);
//        System.out.println(Thread.currentThread().getName());
//        TimeUnit.SECONDS.sleep(Integer.MAX_VALUE);
    }

}
