package com.zzg.jvm.demo;

public class StringDemo {

    public static void main(String[] args) {

        String str1 = "server_names_hash_bucket_size 50;default_type  application/octet-stream;#access_log  logs/access.log  main;client_max_body_size 1024M;upstream mytomcatscluster{server_name  localhost electric.com www.electric.com;server_name  localhost electric.com www.electric.com;ream mytomcatscluster{server_name  localhost electric.com www.electric.com;server_name  localhost electric.com www.electric.com;ream mytomcatscluster{server_name  localhost electric.com www.electric.com;server_name  localhost electric.com www.electric.com;ream mytomcatscluster{server_name  localhost electric.com www.electric.com;server_name  localhost electric.com www.electric.com;";
        String str2 = "server_names_hash_bucket_size 50;default_type  application/octet-stream;#access_log  logs/access.log  main;client_max_body_size 1024M;upstream mytomcatscluster{server_name  localhost electric.com www.electric.com;server_name  localhost electric.com www.electric.com;ream mytomcatscluster{server_name  localhost electric.com www.electric.com;server_name  localhost electric.com www.electric.com;ream mytomcatscluster{server_name  localhost electric.com www.electric.com;server_name  localhost electric.com www.electric.com;ream mytomcatscluster{server_name  localhost electric.com www.electric.com;server_name  localhost electric.com www.electric.com;";
        System.out.println("str1.length() = " + str1.length());
        System.out.println("str2.length() = " + str2.length());
        System.out.println(str1 == str2);

    }

}
