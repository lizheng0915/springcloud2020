package com.zzg.jvm.reference;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

public class WeakHashMapDemo {

    public static void main(String[] args) {
        hashMapTest();
        System.out.println("===============");
        weakHashMapTest();
    }

    private static void weakHashMapTest() {
        WeakHashMap<String, String> map = new WeakHashMap<>();
//        Integer key = new Integer(123);
//        String key = "12345";
        String key = new String("12345");      // String key = "12345"; 跟 new String("12345"); 有区别
        map.put(key, UUID.randomUUID().toString());
        System.out.println("weakmap = " + map);

        key = null;
        System.out.println("key null --> weakmap = " + map);

        System.gc();
        System.out.println("after gc --> weakmap = " + map);

    }

    private static void hashMapTest() {
        Map<String, String> map = new HashMap<>();
        String key = "123456";
        map.put(key, UUID.randomUUID().toString());

        System.out.println("map = " + map);
        key = null;
        System.out.println("key null --> map = " + map);

        System.gc();
        System.out.println("after gc --> map = " + map);
    }

}
