package com.zzg.jvm.reference;

import java.lang.ref.WeakReference;
import java.util.UUID;

/**
 * weak reference --> 弱引用 --> 只要触发了GC，不管内存是否够用都进行回收
 */
public class WeakReferenceDemo {

    public static void main(String[] args) {
        /**
         * 示例中创建obj1对象，创建obj2对象指向obj1的引用，这都是强引用
         * 即便obj1指定为null，然后启动了垃圾回收
         * 但是obj2仍然存在，不会被回收
         * 只要有一个引用还指向一个对象，那么这个堆空间的对象就不会被回收，即便爆发OOM把自己搞死，也不回收
         */
        Object obj1 = new Object();
        WeakReference<Object> weakReference = new WeakReference<>(obj1);
        System.out.println("obj1 = " + obj1);
        System.out.println("before GC --> weakReference.get() = " + weakReference.get());
        obj1 = null;
        System.gc();
        System.out.println("obj1 = " + obj1);
        System.out.println("after GC -->  weakReference2.get() = " + weakReference.get());

        System.out.println("===================================");

        WeakReference<Object> weakReference2 = new WeakReference<>(new Object());
        System.out.println("before GC --> weakReference2.get() = " + weakReference2.get());
        System.gc();
        System.out.println("after GC -->  weakReference2.get() = " + weakReference2.get());

        System.out.println("===================================");

        WeakReference<String> weakReference3 = new WeakReference<>(UUID.randomUUID().toString());
        System.out.println("before GC --> weakReference3.get() = " + weakReference3.get());
        System.gc();
        System.out.println("after GC -->  weakReference3.get() = " + weakReference3.get());
    }

}
