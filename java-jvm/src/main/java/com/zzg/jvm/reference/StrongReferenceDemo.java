package com.zzg.jvm.reference;

/**
 * JAVA强引用不会被回收
 */
public class StrongReferenceDemo {

    public static void main(String[] args) {
        /**
         * 示例中创建obj1对象，创建obj2对象指向obj1的引用，这都是强引用
         * 即便obj1指定为null，然后启动了垃圾回收
         * 但是obj2仍然存在，不会被回收
         * 只要有一个引用还指向一个对象，那么这个堆空间的对象就不会被回收，即便爆发OOM把自己搞死，也不回收
         */
        Object obj1 = new Object();
        Object obj2 = obj1;
        System.out.println("obj1 = " + obj1);
        System.out.println("obj2 = " + obj2);
        obj1 = null;
        System.gc();
        System.out.println("obj1 = " + obj1);
        System.out.println("obj2 = " + obj2);
    }

}
