package com.zzg.jvm.reference;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.concurrent.TimeUnit;

/**
 * 虚引用 --> phantom reference --> 形同虚设的引用，get方法一直返回null，需要跟引用队列一起使用，表示在回收之后要做的事情
 */
public class PhantomReferenceDemo {

    public static void main(String[] args) throws InterruptedException {
        Object obj1 = new Object();
        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
        /**
         * 使用弱引用创建的第二个构造方法，指定在回收之前先放入引用队列
         */
        PhantomReference<Object> phantomReference = new PhantomReference<>(obj1, referenceQueue);

        System.out.println("obj1 = " + obj1);
        System.out.println("phantomReference.get() = " + phantomReference.get());
        System.out.println("referenceQueue.poll() = " + referenceQueue.poll());

        System.out.println("===================After GC==================");
        obj1 = null;
        System.gc();
        TimeUnit.MILLISECONDS.sleep(500);

        System.out.println("obj1 = " + obj1);
        System.out.println("phantomReference.get() = " + phantomReference.get());
        System.out.println("referenceQueue.poll() = " + referenceQueue.poll());
    }

}
