package com.zzg.jvm.reference;

import java.lang.ref.SoftReference;

/**
 * 软引用 - 比强引用弱一部分的引用
 * 内存足够的时候，不进行回收，内存不足的时候会进行回收
 * <p>
 * 使用场景:
 * 假如有一个应用需要大量读取本地图片
 * 1. 如果每次读取都从硬盘读取的话会严重影响性能
 * 2. 如果把所有图片资源都加载到内存又可能会造成内存溢出
 * <p>
 * 此时引用使用软引用可以解决这个问题
 * 设计思路是： 用一个hashMap用来保存图片的地址和响应图片对象关联的引用之间的映射关系
 * 在内存不足的时候，JVM会自动回收这些缓存图片对象所占用的空间，从而避免OOM
 * <p>
 * 造成问题：大量FULL GC可能影响整体性能，并且FULL GC的时间可能还会很长
 */
public class SoftReferenceDemo {

    /**
     * 在内存足够的情况下，软引用在GC之后扔被保留
     */
    public static void softRefMemoryEnough() {
        Object obj1 = new Object();
        SoftReference<Object> softReference = new SoftReference<>(obj1);
        System.out.println("before GC --> obj1 = " + obj1);
        System.out.println("before GC --> softReference.get() = " + softReference.get());

        obj1 = null;
        System.gc();

        System.out.println("after GC --> obj1 = " + obj1);
        System.out.println("after GC --> softReference.get() = " + softReference.get());
    }

    /**
     * 内存不足的情况下，看看软引用在经过GC之后是否被回收
     * 设置堆内存以及GC日志，再启动程序
     * -Xms5m -Xmx5m -XX:+PrintGCDetails
     * 可以看到，当强引用的对象不再引用的时候，软引用对象在GC之后也被回收了
     */
    public static void softRefMemoryNotEnough() {
        Object obj1 = new Object();
        SoftReference<Object> softReference = new SoftReference<>(obj1);
        System.out.println("before GC --> obj1 = " + obj1);
        System.out.println("before GC --> softReference.get() = " + softReference.get());

        obj1 = null;

        try {
            // 创建一个10M的大对象 - 主动去触发OOM - OOM爆发之前会自动进行GC，所以不再需要手动调用
            byte[] bytes = new byte[10 * 1024 * 1024];
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("after GC --> obj1 = " + obj1);
            System.out.println("after GC --> softReference.get() = " + softReference.get());
        }

    }

    public static void main(String[] args) {
//        softRefMemoryEnough();
        softRefMemoryNotEnough();
    }

}
