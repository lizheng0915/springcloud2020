package com.zzg.jvm.reference;


import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

/**
 * 引用队列
 * 弱引用在被回收之前会先放入引用队列
 */
public class ReferenceQueueDemo {

    public static void main(String[] args) throws InterruptedException {
        Object obj1 = new Object();
        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
        /**
         * 使用弱引用创建的第二个构造方法，指定在回收之前先放入引用队列
         */
        WeakReference<Object> weakReference = new WeakReference<>(obj1, referenceQueue);

        System.out.println("obj1 = " + obj1);
        System.out.println("weakReference.get() = " + weakReference.get());
        System.out.println("referenceQueue.poll() = " + referenceQueue.poll());

        System.out.println("===================After GC==================");
        obj1 = null;
        System.gc();
        TimeUnit.MILLISECONDS.sleep(500);

        System.out.println("obj1 = " + obj1);
        System.out.println("weakReference.get() = " + weakReference.get());
        System.out.println("referenceQueue.poll() = " + referenceQueue.poll());
    }

}
