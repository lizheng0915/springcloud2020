package com.zzg.jvm.classloader;

public class Hello {

    static {
        System.out.println("======Hello======static======");
    }

    public Hello() {
        System.out.println("======Hello======");
    }

    public static void main(String[] args) {
        Hello hello = new Hello();
    }

}
