package com.zzg.jvm.classloader;

import com.alibaba.fastjson.JSONObject;

import javax.security.sasl.AuthorizeCallback;

/**
 * 参考：https://www.cnblogs.com/ityouknow/p/5603287.html
 * <p>
 * JAVA的类装载器
 * 启动类装载器 - Bootstrap null - JDK初始版本就带的装载器，用于加载 %JAVA_HOME%/jre/lib/rt.jar 包的内容，这个无法被打印，是一个null
 * 原因是Bootstrap Loader（引导类加载器）是用C语言实现的，找不到一个确定的返回父Loader的方式，于是就返回null。
 * 扩展类装载器 - Extention ExtClassLoader - JDK后期扩展的类装载器，用于加载 %JAVA_HOME%/jre/lib/ext/*.jar 包的内容，是JDK扩展包的
 * 应用程序类装载器 - Application  AppClassLoader - 用于加载用户编写的类，包括classpath包的.class文件和引入的第三方jar包的类
 */
public class HelloWorld extends Hello {

    public static void main(String[] args) {

        /**
         * 查看jdk自带的类的加载器
         */
        System.out.println("================Bootstrap================");
        Object obj = new Object();
        System.out.println(obj.getClass());
        System.out.println(obj.getClass().getClassLoader());

        AuthorizeCallback callback = new AuthorizeCallback("111", "222");
        System.out.println(callback.getClass());
        System.out.println(callback.getClass().getClassLoader());

        /**
         * 查看当前类的加载器
         */
        System.out.println("================Application================");
        HelloWorld helloWorld = new HelloWorld();
        System.out.println(helloWorld.getClass());
        System.out.println(helloWorld.getClass().getClassLoader());
        System.out.println(helloWorld.getClass().getClassLoader().getParent());
        System.out.println(helloWorld.getClass().getClassLoader().getParent().getParent());


        /**
         * 方法二，查看当前类的加载类
         */
        System.out.println();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        System.out.println(loader);
        System.out.println(loader.getParent());
        System.out.println(loader.getParent().getParent());

        /**
         * 查看第三方jar包的类的加载器
         */
        System.out.println();
        JSONObject json = new JSONObject();
        System.out.println(json.getClass());
        System.out.println(json.getClass().getClassLoader());
        System.out.println(json.getClass().getClassLoader().getParent());
        System.out.println(json.getClass().getClassLoader().getParent().getParent());
    }

}
