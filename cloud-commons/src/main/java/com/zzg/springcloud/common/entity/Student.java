package com.zzg.springcloud.common.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student implements Serializable {

    private Long id;

    private String name;

    /**
     * transient 修饰的属性不会被序列化
     */
    private transient String password;

}
