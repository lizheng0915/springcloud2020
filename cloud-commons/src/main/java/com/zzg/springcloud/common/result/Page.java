package com.zzg.springcloud.common.result;

import com.alibaba.fastjson.JSONObject;

/**
 * @author 张志刚  2014年12月9日
 * 分页插件
 */
public class Page {
    /**
     * 当前页码， 从第一页开始
     */
    private int pageNo;

    /**
     * 每页显示数量
     */
    private int pageSize;

    /**
     * 记录总数量
     */
    private int sum;

    /**
     * 页码总数量
     */
    private int pageTotal;

    /**
     * 单页结果集
     */
    private Object resultList;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(int pageTotal) {
        this.pageTotal = pageTotal;
    }

    public Object getResultList() {
        return resultList;
    }

    public void setResultList(Object resultList) {
        this.resultList = resultList;
    }

    public String getPageSql(String sql) {
        StringBuffer sb = new StringBuffer();
        sb.append(sql);
        sb.append(" LIMIT ");
        sb.append((pageNo - 1) * pageSize);
        sb.append(" , ");
        sb.append(pageSize);
        return sb.toString();
    }

    public Page checkAndInitPage(Page page) {
        if (page.getPageNo() == 0) page.setPageNo(1);
        if (page.getPageSize() == 0) page.setPageSize(10);
        return page;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }

    public JSONObject toJSONObject() {
        return JSONObject.parseObject(this.toString());
    }

}
