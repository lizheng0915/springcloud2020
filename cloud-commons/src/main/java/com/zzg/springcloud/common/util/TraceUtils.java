package com.zzg.springcloud.common.util;

import java.util.UUID;

/**
 * @Author 张志刚
 * @Date 2021/8/20
 * @Description 日志服务链路追踪
 */
public class TraceUtils {

    /**
     * 可以在线程初始化的时候直接初始化数据
     */
    private static final ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<String>() {
        @Override
        public String initialValue() {
            return UUID.randomUUID().toString().replace("-", "");
        }
    };

    /**
     * 初始化线程中保存的值
     *
     * @return String
     */
    public static String init() {
        String traceId = UUID.randomUUID().toString().replace("-", "");
        THREAD_LOCAL.set(traceId);
        return traceId;
    }

    /**
     * 获取线程中保存的值
     *
     * @return String
     */
    public static String get() {
        return THREAD_LOCAL.get() == null ? init() : THREAD_LOCAL.get();
    }

    /**
     * 如果线程中的值为空，则将当前值赋值，然后返回
     *
     * @return String
     */
    public static String get(String traceId) {
        return THREAD_LOCAL.get() == null ? set(traceId) : THREAD_LOCAL.get();
    }

    /**
     * 设置线程需要保存的值
     *
     * @return String
     */
    public static String set(String traceId) {
        THREAD_LOCAL.set(traceId);
        return traceId;
    }

    /**
     * 移除线程中保存的值
     */
    public static void remove() {
        THREAD_LOCAL.remove();
    }

}
