package com.zzg.springcloud.common.test;

import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.entity.Student;
import com.zzg.springcloud.common.result.ResultEnum;
import com.zzg.springcloud.common.util.DateUtils;

public class T {

    public static void main(String[] args) {
        System.out.println(getLineInfo());
        StackTraceElement ste2 = new Throwable().getStackTrace()[0];
        System.out.println(ste2.getFileName() + ": Line " + ste2.getLineNumber());
        System.out.println("=========================================================");
        System.out.println(ste2.getFileName() + ": Line " + new Throwable().getStackTrace()[0].getLineNumber());

    }


    public static String getLineInfo() {
        StackTraceElement ste3 = new Throwable().getStackTrace()[0];
        return (ste3.getFileName() + ": Line " + ste3.getLineNumber());
    }

    public static String getLineInfo(StackTraceElement ste4) {
        return (ste4.getFileName() + ": Line " + (ste4.getLineNumber()));
    }

}
