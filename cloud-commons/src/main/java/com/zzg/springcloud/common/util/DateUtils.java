package com.zzg.springcloud.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    /**
     * 定义一些long类型的时间毫秒值
     */
    public static final long MINUTE = 60 * 1000L;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;
    public static final long WEEK = DAY * 7;

    public static final String U_MINUTE = "MINUTE";
    public static final String U_HOUR = "HOUR";
    public static final String U_DAY = "DAY";
    public static final String U_WEEK = "WEEK";
    public static final String U_MONTH = "MONTH";
    public static final String U_YEAR = "YEAR";

    public static final String first_second = " 00:00:00";
    public static final String last_second = " 23:59:59";

    /**
     * 定义一些常用的 format 类型
     */
    public static final String format_default_date = "yyyy-MM-dd";
    public static final String format_default_datetime = "yyyy-MM-dd HH:mm:ss";
    public static final String yyyy_MM_dd = "yyyy-MM-dd";
    public static final String yyyy = "yyyy";
    public static final String yyyyMM = "yyyyMM";
    public static final String yyyyMMdd = "yyyyMMdd";
    public static final String yyyyMMddHH = "yyyyMMddHH";
    public static final String yyyyMMddHHmm = "yyyyMMddHHmm";
    public static final String yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public static final String yyyyMMddHHmmssSSS = "yyyyMMddHHmmssSSS";

    private DateUtils() {
    }

    public static String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_date);
        return sdf.format(new Date());
    }

    public static String getDatetime() {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_datetime);
        return sdf.format(new Date());
    }

    public static String getDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static String getDatetime(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static String getDate(long millisecond) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_date);
        return sdf.format(new Date(millisecond));
    }

    public static String getDatetime(long millisecond) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_datetime);
        return sdf.format(new Date(millisecond));
    }

    public static String getDate(long millisecond, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(millisecond));
    }

    public static String getDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_date);
        return sdf.format(date);
    }

    public static String getDatetime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_datetime);
        return sdf.format(date);
    }

    public static String getDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static Date getDateByStr(String datestr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.parse(datestr);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Date getDateByStr(String datestr) {
        return getDateByStr(datestr, format_default_datetime);
    }

    public static Date getDateByDatetimeStr(String datestr) {
        return getDateByStr(datestr, format_default_datetime);
    }

    public static Date getDateByDateStr(String datestr) {
        return getDateByStr(datestr, format_default_date);
    }

}







