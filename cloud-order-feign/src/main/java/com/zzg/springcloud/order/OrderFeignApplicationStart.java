package com.zzg.springcloud.order;


import com.zzg.springcloud.common.util.DateUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class OrderFeignApplicationStart {

    public static void main(String[] args) {
        SpringApplication.run(OrderFeignApplicationStart.class, args);
        System.out.println(DateUtils.getDatetime() + "\t======OrderFeignApplicationStart    启动成功======");
    }

}
