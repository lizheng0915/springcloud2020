package com.zzg.springcloud.order.api;

import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.result.ReturnResult;
import com.zzg.springcloud.common.util.DateUtils;
import com.zzg.springcloud.order.feignclient.AutoplayClient;
import com.zzg.springcloud.order.feignclient.PaymentClient;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.UUID;

@RestController
@RequestMapping("order")
@Slf4j
public class OrderFeignController {

    @Value("${server.port}")
    private String serverPort;
    @Value("${spring.application.name}")
    private String applicationName;

    @Resource
    private PaymentClient paymentClent;
    @Resource
    private AutoplayClient autoplayClient;

    // http://127.0.0.1:8083/order/feign/current
    @GetMapping(value = "/feign/current")
    public ReturnResult current() {
        System.out.println(this.getClass().getName() + "-->current");
        JSONObject res = new JSONObject();
        res.put("server.port", serverPort);
        res.put("spring.application.name", applicationName);
        res.put("requestId", UUID.randomUUID().toString());
        res.put("datatime", DateUtils.getDatetime());
        return ReturnResult.get(res);
    }


    // http://127.0.0.1:8083/order/feign/getPayment/123
    @RequestMapping(value = "/feign/getPayment/{id}")
    public ReturnResult getPayment(@PathVariable("id") String id) {
        System.out.println(this.getClass().getName() + "-->getPayment : id = " + id);
        ReturnResult result = this.paymentClent.get(id);
        JSONObject json = result.getData(JSONObject.class);
        return ReturnResult.get(json);
    }

    // http://127.0.0.1:8083/order/feign/postPayment/123
    @PostMapping(value = "/feign/postPayment/{id}")
    public ReturnResult postPayment(@PathVariable("id") String id, @RequestBody JSONObject params) {
        System.out.println(this.getClass().getName() + "-->postPayment : id = " + id);
        ReturnResult result = this.paymentClent.post(id, params);
        JSONObject json = result.getData(JSONObject.class);
        return ReturnResult.get(json);
    }

    // http://127.0.0.1:8083/order/feign/discovery
    @GetMapping(value = "/feign/discovery")
    public ReturnResult discovery() {
        System.out.println(this.getClass().getName() + "-->discovery");
        ReturnResult result = this.paymentClent.discovery();
        return result;
    }


    // http://127.0.0.1:8083/order/feign/sleep/123
    @GetMapping(value = "/feign/sleep/{million}")
    public ReturnResult sleep(@PathVariable("million") Long million) {
        System.out.println(this.getClass().getName() + "-->sleep");
        Request.Options options = new Request.Options(3000, 3000);
        System.out.println("connectTimeoutMillis --> " + options.connectTimeoutMillis());
        System.out.println("readTimeoutMillis --> " + options.readTimeoutMillis());
        System.out.println("isFollowRedirects --> " + options.isFollowRedirects());
        ReturnResult result = this.paymentClent.sleep(options, million);
        return result;
    }

    // http://127.0.0.1:8083/order/feign/sleep2?million=123
    @GetMapping(value = "/feign/sleep2")
    public ReturnResult sleep2(Long million) {
        System.out.println(this.getClass().getName() + "-->sleep2");
        ReturnResult result = this.paymentClent.sleep2(million);
        return result;
    }


    // http://127.0.0.1:8083/order/feign/runstatus??date=2021-05-30&pageNo=1&pageSize=50
    @GetMapping(value = "/feign/runstatus")
    public ReturnResult runstatus(@RequestParam("date") String date, @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        log.info("className = {}, methodName = {}, date = {}", this.getClass().getName(), "runstatus", date);
        ReturnResult result = this.autoplayClient.runstatus(date, pageNo, pageSize);
        return result;
    }
}
