package com.zzg.springcloud.order.api;

import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.result.ReturnResult;
import com.zzg.springcloud.common.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.cms.PasswordRecipientId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@RestController
@RequestMapping("order")
@Slf4j
public class OrderRibbonController {

    @Value("${server.port}")
    private String serverPort;
    @Value("${spring.application.name}")
    private String applicationName;
    @Value("${ribbon.ConnectTimeout}")
    private String ribbonConnectTimeout;
    @Value("${ribbon.ReadTimeout}")
    private String ribbonReadTimeout;

    // 使用 ribbon + restTemplate 调用微服务
    @Autowired
    private RestTemplate restTemplate;

    @Resource
    private HttpServletRequest request;

    private String paymentBaseUrl = "http://CLOUD-PAYMENT";


    // http://127.0.0.1:8082/order/ribbon/current
    @GetMapping(value = "/ribbon/current")
    public ReturnResult current() {
        System.out.println(this.getClass().getName() + "-->current");
        log.info("className = {}, methodName = {}, requestUrl = {}", this.getClass().getSimpleName(), "current", request.getRequestURL());
        JSONObject res = new JSONObject();
        res.put("server.port", serverPort);
        res.put("spring.application.name", applicationName);
        res.put("requestId", UUID.randomUUID().toString());
        res.put("datatime", DateUtils.getDatetime());
        return ReturnResult.get(res);
    }

    // http://127.0.0.1:8082/order/ribbon/getPayment/123
    @RequestMapping(value = "/ribbon/getPayment/{id}")
    public ReturnResult getPayment(@PathVariable("id") String id) {
        System.out.println(this.getClass().getName() + "-->getPayment : id = " + id);
        log.info("className = {}, methodName = {}, requestUrl = {}", this.getClass().getSimpleName(), "getPayment", request.getRequestURL());
        String url = paymentBaseUrl + "/payment/get/" + id;
        ReturnResult result = this.restTemplate.getForObject(url, ReturnResult.class);
        JSONObject json = result.getData(JSONObject.class);
        return ReturnResult.get(json);
    }

    // http://127.0.0.1:8082/order/ribbon/payment/sleep/123
    @RequestMapping(value = "/ribbon/payment/sleep/{million}")
    public ReturnResult paymentSleep(@PathVariable("million") Long million) {
        System.out.println(this.getClass().getName() + "-->paymentSleep : million = " + million);
        log.info("className = {}, methodName = {}, requestUrl = {}", this.getClass().getSimpleName(), "paymentSleep", request.getRequestURL());
        String url = paymentBaseUrl + "/payment/sleep/" + million;
        ReturnResult result = this.restTemplate.getForObject(url, ReturnResult.class);
        JSONObject json = result.getData(JSONObject.class);
        json.put("ribbonReadTimeout", ribbonReadTimeout);
        json.put("ribbonConnectTimeout", ribbonConnectTimeout);
        return ReturnResult.get(json);
    }

    // http://127.0.0.1:8082/order/ribbon/payment/sleep2?million=1234
    @RequestMapping(value = "/ribbon/payment/sleep2")
    public ReturnResult paymentSleep2(Long million) {
        System.out.println(this.getClass().getName() + "-->paymentSleep2 : million = " + million);
        log.info("className = {}, methodName = {}, requestUrl = {}", this.getClass().getSimpleName(), "paymentSleep2", request.getRequestURL());
        String url = paymentBaseUrl + "/payment/sleep2?million=" + million;
        ReturnResult result = this.restTemplate.getForObject(url, ReturnResult.class);
        JSONObject json = result.getData(JSONObject.class);
        json.put("ribbonReadTimeout", ribbonReadTimeout);
        json.put("ribbonConnectTimeout", ribbonConnectTimeout);
        return ReturnResult.get(json);
    }

}
