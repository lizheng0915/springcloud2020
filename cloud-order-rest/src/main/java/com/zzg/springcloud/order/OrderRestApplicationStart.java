package com.zzg.springcloud.order;


import com.zzg.springcloud.common.util.DateUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class OrderRestApplicationStart {

    public static void main(String[] args) {
        SpringApplication.run(OrderRestApplicationStart.class, args);
        System.out.println(DateUtils.getDatetime() + "\t======OrderApplicationStart    启动成功======");
    }

}
