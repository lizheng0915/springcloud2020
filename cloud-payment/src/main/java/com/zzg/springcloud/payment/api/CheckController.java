package com.zzg.springcloud.payment.api;

import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.result.ReturnResult;
import com.zzg.springcloud.common.util.DateUtils;
import com.zzg.springcloud.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Priority;

@RestController
@RequestMapping(value = "check")
@Primary
@Priority(1)
public class CheckController {

    // http://127.0.0.1:18001/check/test
    @RequestMapping(value = "/test")
    @ResponseBody
    @Autowired
    @Qualifier
    public ReturnResult test() {
        String str = DateUtils.getDatetime() + "-->" + this.getClass().getName();
        str += "-->" + Thread.currentThread().getStackTrace()[1].getMethodName();
        System.out.println(Thread.currentThread().getName());
        System.out.println(Thread.currentThread().getStackTrace());
        System.out.println(Thread.currentThread().getClass().getSimpleName());
        System.out.println(Thread.currentThread().getClass());
        System.out.println(str);
        System.out.println("======");
        for (StackTraceElement st : Thread.currentThread().getStackTrace()) {
            System.out.println(JSONObject.toJSONString(st));
        }
        System.out.println("======");
        System.out.println(Thread.currentThread().getStackTrace()[1].getClassName());
        System.out.println("===Thread.currentThread().getStackTrace()[1]===");
        StringHelper.log(Thread.currentThread().getStackTrace()[1]);
        System.out.println("===Thread.currentThread().getStackTrace()===");
        StringHelper.log(Thread.currentThread().getStackTrace());
        ReturnResult result = ReturnResult.getSuccessResult();
        JSONObject json = new JSONObject();
        json.put("datetime", DateUtils.getDatetime());
        json.put("getName", this.getClass().getName());
        json.put("getMethodName", Thread.currentThread().getStackTrace()[1].getMethodName());
        json.put("name", "张志刚");
        result.setData(json);
        return result;
    }

}
