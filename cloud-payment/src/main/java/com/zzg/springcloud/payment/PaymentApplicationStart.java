package com.zzg.springcloud.payment;

import com.zzg.springcloud.common.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@Slf4j
public class PaymentApplicationStart {
    public static void main(String[] args) {
        SpringApplication.run(PaymentApplicationStart.class, args);
        log.info("======PaymentApplicationStart    启动成功======");
        System.out.println(DateUtils.getDatetime() + "\t======PaymentApplicationStart    启动成功======");
    }
}
