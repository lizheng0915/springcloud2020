package com.zzg.base.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class ReflectUtils {


    /**
     * 要通过反射获取的对象，必须提供一个无参构造方法
     *
     * @param clazz
     */
    public static Object getObject(Class clazz) {
        try {
            // 获取无参的构造方法 - 为保证私有的和公共的都可以获取，使用 getDeclaredConstructor() 方法
            Constructor con = clazz.getDeclaredConstructor();
            con.setAccessible(true);    // 如果是私有方法，需要设置为true才能在反射中调用
            return con.newInstance();
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static Object getObject(String className) {
        try {
            Class clazz = Class.forName(className);
            return getObject(clazz);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static <T> T mapToBean(Map dataMap, Class<T> clazz) throws IllegalAccessException, InstantiationException {
        Object obj = clazz.newInstance();

        return (T) obj;
    }

}
