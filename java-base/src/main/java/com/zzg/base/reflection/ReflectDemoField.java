package com.zzg.base.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 反射技术中对成员变量的了解
 * ## 获取成员变量 - 各个方法可以获取的结果范围
 * - getFields 所有public修饰的成员变量，以及继承自父类的public修饰的成员变量 - 只能是 public 修饰符
 * - getDeclaredFields 当前对象的所有成员变量，各种修饰符修饰的均可 - 除却继承自父类的数量，即便是 public 修饰的也不行
 * - getField(String name) 根据 name 在 getFields 内查找
 * - getDeclaredField(String name) 根据 name 在 getDeclaredFields 结果集内查找
 */
public class ReflectDemoField {

    public static void main(String[] args) throws Exception {
//        getFieldsTest(ReflectEntity.class);
//        System.out.println("=========================================");
//        getDeclaredFieldsTest(ReflectEntity.class);
//        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        fieldTest(ReflectEntity.class);
    }

    // 获取和修改成员变量
    public static void fieldTest(Class clazz) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        // 根据 Class 对象获取对象实例
        Object obj = ReflectUtils.getObject(clazz);

        // 根据单个公有成员变量 - 获取变量值 - 修改变量值 - 获取修改后的值
        // public final static 修饰的变量
        Field CHARSET_NAME = clazz.getField("CHARSET_NAME");
        CHARSET_NAME.setAccessible(true);
        Object CHARSET_NAME_RES = CHARSET_NAME.get(obj);
        System.out.println("CHARSET_NAME_RES = " + CHARSET_NAME_RES);
//        CHARSET_NAME.set(obj, "GBK");       // final 修饰的变量不能被修改
        System.out.println("CHARSET_NAME_SET_AFTER = " + CHARSET_NAME.get(obj));

        // private static 修饰的变量
        Field DEFAULT_DATE_FORMAT = clazz.getDeclaredField("DEFAULT_DATE_FORMAT");
        DEFAULT_DATE_FORMAT.setAccessible(true);
        Object DEFAULT_DATE_FORMAT_RES = DEFAULT_DATE_FORMAT.get(obj);
        System.out.println("DEFAULT_DATE_FORMAT_RES = " + DEFAULT_DATE_FORMAT_RES);
        DEFAULT_DATE_FORMAT.set(obj, "yyyyMMddHHmmss");
        System.out.println("DEFAULT_DATE_FORMAT_RES = " + DEFAULT_DATE_FORMAT.get(obj));

        // final static List<String> 获取其变量结果，以及调用变量的方法
        Field default_final_static_field = clazz.getDeclaredField("default_final_static_field");
        default_final_static_field.setAccessible(true);
        Object default_final_static_field_res = default_final_static_field.get(obj);
        System.out.println("default_final_static_field_res = " + default_final_static_field_res);
        System.out.println("default_final_static_field_res.getClass() = " + default_final_static_field_res.getClass());

        Method listGetMethod = default_final_static_field_res.getClass().getMethod("get", int.class);
        listGetMethod.setAccessible(true);
        System.out.println("listGetMethod.invoke(default_final_static_field_res, 1) = " + listGetMethod.invoke(default_final_static_field_res, 1));
    }

    /**
     * 获取所有的成员变量 - 范围 - 当前类定义的任意修饰符修饰的均可获得 - 除去继承自父类的变量
     */
    public static void getDeclaredFieldsTest(Class clazz) {
        Field[] fields = clazz.getDeclaredFields();
        printFieldInfo(fields);
    }

    /**
     * 获取公有的成员变量 - 范围 - public 修饰符修饰的成员变量以及继承自父类的成员变量
     */
    public static void getFieldsTest(Class clazz) {
        Field[] fields = clazz.getFields();
        printFieldInfo(fields);
    }

    private static void printFieldInfo(Field[] fields) {
        String format = "Field Info : name = %-30s; modifiers = %d; string = %s;";
        for (Field field : fields) {
            System.out.println(String.format(format, field.getName(), field.getModifiers(), field));
        }
    }

}
