package com.zzg.base.reflection;


import java.lang.annotation.ElementType;

/**
 * java中所有对象都会有 Class 属性
 */
public class ReflectDemoClass {

    public static void main(String[] args) throws ClassNotFoundException {
        // 各种数据类型获取 Class 对象
        showClass();

        // 各种方法获取 Class 对象，反射的主要依据就是 Class 对象
        classTest();
    }

    /**
     * 展示数据类型的Class，所有数据类型均可通过 .class 来获取 Class 对象
     */
    public static void showClass() {
        Class c1 = Object.class;        // 类和对象的Class
        Class c2 = Comparable.class;    // 接口的Class
        Class c3 = String[].class;      // 一维数组
        Class c4 = int[][].class;       // 二维数组
        Class c5 = Override.class;      // 注解
        Class c6 = ElementType.class;   // 枚举
        Class c7 = Integer.class;       // 级别数据类型
        Class c8 = void.class;          // 空数据类型
        Class c9 = Class.class;         // Class的Class
    }

    /**
     * 三种方法获取Class对象
     * 反射都是一句 Class 对象进行的
     */
    public static void classTest() throws ClassNotFoundException {
        // 方法一: 很少使用的方法 - 通过具体对象来获取 - 对象.getClass()
        ReflectEntity reflectEntity = new ReflectEntity("解放军");
        Class clazz1 = reflectEntity.getClass();
        System.out.println("clazz1 = " + clazz1);

        // 方法二: 通过类来获取  类名.class
        Class clazz2 = ReflectEntity.class;
        System.out.println("clazz2 = " + clazz2);

        // 方法三: 完整的包名和类名  Class.forName("完整的包名和类名");  这种方式比较常用
        Class clazz3 = Class.forName("com.zzg.base.reflection.ReflectEntity");
        System.out.println("clazz3 = " + clazz3);

        // 表明 : 一个类在内存中只有一个Class对象
        // 一个类被加载后，类的整个结构都被封装在Class对象中
        System.out.println("=================查看对象hashCode==============");
        System.out.println("clazz1.hashCode() = " + clazz1.hashCode());
        System.out.println("clazz2.hashCode() = " + clazz2.hashCode());
        System.out.println("clazz3.hashCode() = " + clazz3.hashCode());

        // equals 比较两个对象的 hashCode 的值; == 比较两个值在内存中的地址
        System.out.println("clazz1.equals(clazz2) = " + clazz1.equals(clazz2));
        System.out.println("clazz1 == clazz2 = " + (clazz1 == clazz2));

    }
}
