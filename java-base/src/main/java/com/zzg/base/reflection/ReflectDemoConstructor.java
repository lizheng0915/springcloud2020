package com.zzg.base.reflection;


import java.lang.reflect.Constructor;
import java.util.Date;

/**
 * 反射技术中构造方法的了解
 * - getConstructors : 获取所有公共构造函数
 * - getDeclaredConstructors : 获取所有构造函数，私有 + 公有
 * - getConstructor(Class... ) : 根据参数在 getConstructors 的结果集范围内查找
 * - getDeclaredConstructor(Class...) : 根据参数在 getDeclaredConstructors 的结果集范围内查找
 */
public class ReflectDemoConstructor {


    public static void main(String[] args) throws Exception {

        // 对构反射构造方法的认识和学习
//        ReflectDemoConstructor.constructorTest(ReflectEntity.class);

//        ReflectEntity obj = ReflectEntity.class.newInstance();
//        System.out.println(obj);
    }


    /**
     * 构造方法测试
     * Class -> 对象的 Class
     * 1. 比如 Spring 要求要求存在一个无参的构造方法，可能就是用来构建对象的
     * 2. 可以在参数Class上使用泛型，那么调用 newInstance() 方法就可以直接返回指定类型的对象
     */
    public static void constructorTest(Class clazz) throws Exception {
        // 获取所有的构造方法(包括私有的)
        System.out.println("=====构造方法列表=====");
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            System.out.println("Constructor Method Info is : " + constructor);
        }

        System.out.println("=====通过clazz.newInstance()建对象=====");
        Object object = clazz.newInstance();    // 如果对象包含无参、公有构造方法，则可直接使用 clazz.newInstance(); 来创建对象

        System.out.println("=====通过构造方法创建对象=====");
        // 通过构造方法创建对象 - 无参构造方法 - Spring框架要求依赖注入的对象要有一个无参的构造方法，可能就是用来创建要依赖的对象的
        // 如果一个对象没有提供任何构造方法，则JVM默认给提供一个无参构造方法，如果提供了构造方法，则无参构造方法需自己提供
        Constructor conn1 = clazz.getConstructor();   // getConstructor 获取公有的无参的构造方法
        Object obj1 = conn1.newInstance();            // 通过无参构造方法创建对象
        System.out.println("obj1 = " + obj1);
        ReflectEntity entity1 = (ReflectEntity) obj1;    // 强制转换对象
        System.out.println("entity1 = " + entity1);
        entity1.setName("张志刚");                       // 调用方法设置属性值
        System.out.println("entity1 = " + entity1);

        System.out.println("=====通过有参构造方法创建对象=====");
        // 通过构造方法创建对象 -- 有参数的构造方法
        Constructor cons = clazz.getDeclaredConstructor(Integer.class, String.class);
        cons.setAccessible(true);                               // 私有方法需要设置可访问为 true - 尽可能在所有方法调用前都设置此属性
        Object obj = cons.newInstance(15, "张志刚15");   // 通过有参构造方法创建对象
        System.out.println("obj = " + obj);
        ReflectEntity entity = (ReflectEntity) obj;
        entity.setBirthday(new Date());
        System.out.println("entity = " + entity);
        System.out.println("entity.add(100, 200) = " + entity.add(100, 200));
    }

}
