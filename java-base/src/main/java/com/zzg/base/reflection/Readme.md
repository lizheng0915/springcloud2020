
## 反射（reflect）的概述
JAVA的反射机制是在运行状态中，对于任意一个类，都能够知道这个类的所有属性和方法；对于任意一个对象，都能够调用它的任意一个方法和属性（包括私有的方法和属性）；
这种动态获取的信息以及动态调用方法的功能称之为JAVA的反射机制。

反射机制是框架设计的灵魂，几乎所有的框架都会用到反射技术；JAVA中的所有类在反射技术面前都是裸体。
一个类在内存中只会有一个Class对象，所以通过各种方法返回的同一个类的Class对象其实是一致的。

在运行状态中: 
- 对于任意一个类，都能够知道这个类的所有方法和属性
- 对于任意一个对象，都能够调用它的任意一个属性和方法

反射使用的前提条件是必须得到代表的字节码Class。


## 获取反射的对象
- 方法一 : 通过 对象名.getClass(); 这种方式使用的比较少
- 方法二 : 通过 类名.class; 来获取
- 方法三 : Class.forName("完整的包名+类名"); 这种方式比较常用

## 创建类


## 获取构造方法
- public Constructor<?>[] getConstructors(); 获取所有的公有的构造方法; 不包括私有方法
- public Constructor<?>[] getDeclaredConstructors(); 获取所有的构造方法(包括私有的)
- public Constructor getConstructor(Class<?>… parameterTypes); 获取单个公有的构造方法，参数是对应的构造方法的参数类型列表，只能是公有的
- public Constructor getDeclaredConstructor(Class<?>… parameterTypes); 获取单个私有的构造方法，参数同上，可以是公有的和私有的

根据构造方法，可以通过 Constructor 对象的 newInstance(Object ... initargs) 获取创建一个对象。

## 获取成员方法
- public Method[] getMethods();  获取所有的公有的方法(包括父类公有的方法)
- public Method[] getDeclaredMethods();     获取所有的方法(包括私有)，但是不包括继承自父类的公有方法
- public Method getMethod(String name, Class<?>… parameterTypes);  获取单个公有的方法（静态的+非静态的），name是方法名字，Class... 是方法参数的数据类型列表
- public Method getDeclaredMethod(String name, Class<?>… parameterTypes);  获取单个方法（公有的+私有的+静态的+非静态的），无法获取父类的方法

## 获取成员变量
- public Field[] getFields();  获取公有的成员变量
- public Field[] getDeclaredFields();  获取所有的成员变量
- public Field getField(String name);  获取单个成员变量
- public Field getDeclaredField(String name); 获取单个成员变量


## 参考
Java反射
https://blog.csdn.net/qq_48642210/article/details/123426458

Java基础之—反射（非常重要）
https://blog.csdn.net/sinat_38259539/article/details/71799078


