package com.zzg.base.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * ## 获取成员方法 - 各个方法的结果范围
 * - getMethods 本类的公有的静态和非静态方法 + 父类的静态和非静态方法 - 不包括 private/protected/default 修饰的方法
 * - getDeclaredMethods 本类的中定义的所有方法（public/pricate/protected/default/static） -    不包括父类的方法
 * - getMethod          获取具体的一个方法; 根据名字和参数在 getMethods 结果集的范围内查找
 * - getDeclaredMethod  获取具体的一个方法; 根据名字和参数在 getDeclaredMethods 结果集的范围内查找
 */

public class ReflectDemoMethod {

    public static void main(String[] args) throws Exception {
        getMethodsTest(ReflectEntity.class);
        System.out.println("======================================");
        getDeclaredMethodsTest(ReflectEntity.class);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        methodTest(ReflectEntity.class);
    }


    /**
     * 测试反射对象中的各种方法的使用
     * 1. 通过构造方法创建实例对象
     * 2. 通过clazz获取想要执行的方法
     * 3. 通过 Method 对象的 invoke() 方法，达到实际调用方法的效果
     */
    public static void methodTest(Class clazz) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // 根据 Class 对象获取对象实例
        Object obj = ReflectUtils.getObject(clazz);

        // 获取对象的私有静态方法 + 方法的调用
        Method staticPrivateMethod = clazz.getDeclaredMethod("staticPrivateMethod", String.class);
        staticPrivateMethod.setAccessible(true);
        Object invokeRes = staticPrivateMethod.invoke(obj, "张志刚");
        System.out.println("invokeRes = " + invokeRes);

        // 获取对象的属性设置方法 - 修改对象属性值
        Method setBirthday = clazz.getDeclaredMethod("setBirthday", Date.class);
        Object setBirthdayRes = setBirthday.invoke(obj, new Date());
        System.out.println("setBirthdayRes = " + setBirthdayRes);
        System.out.println("obj = " + obj);
    }


    /**
     * getMethods - 可以获取所有的自己公有的 + 继承自父类的公有方法； 静态的 + 非静态的
     */
    public static void getMethodsTest(Class clazz) {
        Method[] methods = clazz.getMethods();
        printMethodInfo(methods);
    }

    /**
     * getDeclaredMethods - 可以当前类所有的方法
     */
    public static void getDeclaredMethodsTest(Class clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        printMethodInfo(methods);
    }

    private static void printMethodInfo(Method[] methods) {
        String format = "Method Info : name = %-20s; modifiers = %d; string = %s;";
        for (Method method : methods) {
            System.out.println(String.format(format, method.getName(), method.getModifiers(), method));
        }
    }


}
