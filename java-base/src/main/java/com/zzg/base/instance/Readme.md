
## instanceof

instanceof 是JAVA的保留关键字，可以用于判断对象实例跟某一个对象是否有继承关系。

- 子类 instanceof 父类 -- true
- 父类 instanceof 子类 -- false
- 一个父类的两个子类相互 instanceof -- false

