
## 元注解
元注解的作用就是用来注解其他注解，JAVA定义了4个标准的元注解（meta-annotation）,他们用来对其他注解进行类型说明。
在JAVA定义的注解或者自定义的注解中经常用到。
- @Target : 用于描述注解的使用范围
- @Retention : 表示需要在什么级别保存该注释信息。用于描述注解的生命周期。（SOURCE < CLASS < RUNTIME）
- @Documented : 说明该注解被包含在javadoc中
- @Inherited : 说明子类可以继承父类中的该注解


## 自定义注解
使用 @interface 自定义注解时，自动继承了 java.lang.annotation.Annotation 接口。

- @interface 用来生命一个注解，格式是 public @interface AnnotationName{}
- 其中的每一个方法，实际是声明了一个配置参数
- 方法的名称就是参数的名称。 String name(); 这里面name就是注解的参数; 在注解里面看起来跟一个方法一样，其实是注解的参数
- 返回值类型就是参数的类型（返回值只能是基本类型 + Class + String + enum）
- 可以通过 default 来声明参数的默认值
- 如果只有一个参数成员，一般参数名为 value
- 参数成员必须要有值，我们在定义参数成员时，经常使用空字符串，0作为默认值


## 参考
Java注解
https://blog.51cto.com/u_15127699/3908149

Java-Java5.0注解解读
https://blog.51cto.com/u_15239532/5203446