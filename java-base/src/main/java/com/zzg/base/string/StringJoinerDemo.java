package com.zzg.base.string;


import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

/**
 * StringJoiner 是 JDK 1.8 推出一个字符串合并的工具类
 * 1. 解决循环数字或者集合是组成的字符串
 * 2. 内部使用的是 StringBuilder 来实现的，所以不是线程安全的工具方法，使用时需要注意
 */
public class StringJoinerDemo {

    public static void main(String[] args) {

        /**
         * 在以往如果要对一个集合进行循环，并且对其中的符合条件的字符串进行拼接的时候
         * 需要使用String的 += 或者 StringBuffer 的 append 方法进行处理，并且在结尾处要对多余的拼接符号进行截断
         * 有了 StringJoiner 可以方便的解决这个问题
         */

        List<String> list = Arrays.asList("1,2,3,4,5,6,7,8,10".split(","));
        /**
         * joiner 支持2个构造方法，
         * 只一个参数时，表示分隔符
         * 三个参数时候，表示分隔符、前缀、后缀
         */
        StringJoiner joiner = new StringJoiner(";", "{", "}");
        for (String str : list) {
            joiner.add(str);
        }
        // 正常拼接所有结果
        System.out.println("joiner = " + joiner.toString());

        joiner = new StringJoiner(";", "{", "}");
        for (String str : list) {
            int temp = Integer.parseInt(str);
            if (temp % 2 == 0) joiner.add(str);
        }
        // 拼接符合条件的结果
        System.out.println("joiner = " + joiner);

        joiner = new StringJoiner(";", "{", "}");
        for (String str : list) {
            if (str.length() > 11) joiner.add(str);
        }
        // 没有符合条件的结果
        System.out.println("joiner = " + joiner);
    }
}
