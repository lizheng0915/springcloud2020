package com.zzg.base.string;

import java.util.Date;

/**
 * String.format()的详细用法 : https://blog.csdn.net/anita9999/article/details/82346552/
 * 1. 对字符串进行格式化显示，替换指定参数
 * 2. 字符串类型、整数类型、浮点数类型、日期类型等
 * 3. String.format 的底层是基于 java.util.Formatter 对象来实现的
 * <p>
 * 日期类型可参考 : https://blog.csdn.net/qq_37279783/article/details/103914371
 */
public class StringFormatDemo {

    public static void main(String[] args) {

        /**
         * ========字符串======== 可应用于任何参数类型
         * %s = 字符串类型; format 格式中标记为字符串, 那么参数里面可以使用任何类型的数据输出;
         * %c = 字符类型; 一个char类型字符
         * %b = 布尔类型; 输出一个boolean类型结果
         * %n = 换行符(产生特定于平台的行分隔符)
         * %% = 输出百分号 - %%s 不属于格式化符号，会直接输出 %s 字符串，其他所有参数也都一样
         * %4s   : 表示最少占用4个字符的空间，如果参数长度不够，则需要在前面补充空格
         * %-9s  : 表示最少占用9个字符的空间，如果参数长度不够，则需要在后面补充空格
         * %1$s  : 可以通过指定参数位置; 1$表示此次要替换的是第一个参数, 2$ 表示第二个参数，以此类推，从1开始；
         *         通过此方法，可以灵活指定参数位置，同时如果要替换的位置内容相同，可以少输入部分参数
         *         但是需要注意的是: 未指定位置的格式，仍将从第一个参数开始套用。
         */
        String pattern = "%4s你的名字: %s; 你的年龄: %4s; 你的工资: %-9s; 生日: %s; 还能输出一个字符: %c; 布尔类型: %b; %%s";
        System.out.println(String.format(pattern, "", "张志刚", 33, 555.34, new Date(), 'A', "1".equals("sb")));

        pattern = "用户姓名: %1$s; 来自: %2$s; 年龄 %3$d; 性别 %s;";   // 最后的性别没有指定参数位置，所以又从第一个参数开始了
        System.out.println(String.format(pattern, "张志刚", "上海", 23, "男"));


        /**
         * ========整数======== 可应用于 Java 的整数类型：byte、Byte、short、Short、int、Integer、long、Long 和 BigInteger
         * %d = 整数类型（十进制）; format 格式中标记为数字, 参数只能是对应的格式, 整数、浮点数等都不能混用
         * %+d   :  为正数或者负数添加符号, 输入15, 返回 +15
         * %6d   :  表示最少占用6个字符的空间，如果参数长度不够，则需要在前面补充空格
         * %-6d  :  表示最少占用6个字符的空间，如果参数长度不够，则需要在后面补充空格
         * %06d  :  表示最少占用6个字符的空间，如果参数长度不够，则需要在前面补充 0
         * %,d   :  以“,”对数字分组(常用显示金额)
         *
         * %x = 整数类型（十六进制）; 可以把输入的10进制转换成整数的16进制输出 -- 调用 : Integer.toHexString(999));
         * %a = 浮点数类型（十六进制）; 可以把输入的10进制转换成16进制的小数输出 -- 调用 : Double.toHexString(999.999));
         * %o = 整数类型（八进制）;   可以把输入的10进制转换成8进制输出  -- 调用 : Integer.toOctalString(100)
         */
        pattern = "整数类型: [%d]; 显示符号&补充位数: [%0+6d]; 固定长度整数: [%-6d]; 固定长度补充0: [%06d]; 分割显示: [%,d]; 分割补充显示: [%,015d]";
        System.out.println(String.format(pattern, 35, 35, -100, 111, Integer.MAX_VALUE, Integer.MAX_VALUE));

        pattern = "999的十六进制是: [%05x]; 999.99的十六进制浮点数: [%a]; 100的八进制是: [%o]; 555555F的指数是: [%e]";
        System.out.println(String.format(pattern, 999, 999.99, 100, 555555F));

        /**
         * ========浮点数======== 可用于 Java 的浮点类型：float、Float、double、Double 和 BigDecimal
         * %f = 浮点数类型（Float 和 Double）
         * %,f   : 使用,分割显示(常用显示金额)
         * %.2f  : 对小数点指定小数点后长度的处理显示，处理逻辑是四舍五入; .2 表示小数点后面保留2位数字, 如果后面不足2个位数则补0
         * %010.2f : 表示总长度占用10个字符, 长度不够前面补0, 小数点后面保留2位小数
         * %e = 指数类型; 输入的只能是 double 或者 float 类型，不能是 int 和 long 类型
         */
        pattern = "浮点类型: [%f]; 分割显示: [%,f]; 分割补充显示: [%,015f]; 小数点位数: [%010.2f]; 保留小数点位数: [%.3f]";
        System.out.println(String.format(pattern, 444.232, 999999.99, 99999.999, 888.8888, 999F));

        pattern = "指数类型: [%e]; 指数类型: [%e]";
        System.out.println(String.format(pattern, 555555f, 999999.909D));

        /**
         * https://blog.csdn.net/qq_37279783/article/details/103914371
         * ========日期格式========可应用于 Java 的、能够对日期或时间进行编码的类型：long、Long、Calendar 和 Date。
         * %tx = 日期与时间类型（x代表不同的日期与时间转换符)
         *
         */
        Date date = new Date();
        System.out.println(String.format("日期格式: 全部日期和时间信息: [%tc]", date));
        System.out.println(String.format("日期格式: 年-月-日格式: [%20tF]", date));
        System.out.println(String.format("日期格式: 月/日/年格式: [%20tD]", date));
        System.out.println(String.format("日期格式: HH:MM:SS PM格式（12时制）格式: [%tr]", date));
        System.out.println(String.format("日期格式: HH:MM:SS格式（24时制）格式: [%tT]", date));
        System.out.println(String.format("日期格式: HH:MM格式（24时制）格式: [%tR]", date));
        System.out.println(String.format("日期格式: 年格式: [%tY]", date));
        System.out.println(String.format("日期格式: 月格式: [%tm]", date));
        System.out.println(String.format("日期格式: 日格式: [%te]", date));
        System.out.println(String.format("日期格式: 组合使用格式: [%1$tF %1$tT]", date));
        System.out.println(String.format("日期格式: 组合使用格式: [%1$tF %1$tT]", date.getTime()));

        pattern = "日期格式: 组合使用: [%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS,%1$tL]";
        System.out.println(String.format(pattern, date));

        /**
         * 数字的进制转换
         */
        System.out.println("Integer.toOctalString(100) = " + Integer.toOctalString(100));
        System.out.println("Integer.toHexString(999) = " + Integer.toHexString(999));
        System.out.println("Integer.toBinaryString(100) = " + Integer.toBinaryString(100));
        System.out.println("Double.toHexString(999.999) = " + Double.toHexString(999.999));
    }


}
