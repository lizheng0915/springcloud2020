package com.zzg.base.generics.entity;

import com.zzg.base.entity.Teacher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * 自定义类附带泛型 - 在类上不声明泛型，需要在使用的方法中声明泛型
 */
public class GenericsEntity1 {

    private Integer code;
    private String message;
    private Object data;

    /**
     * getter/setter方法
     */
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 返回指定泛型的内容格式，<> 里面的内容是对泛型的声明，没有声明则无法使用
     * 可以实现，无论往data里面设置什么类型的数据
     * 在接受的时候声明了类型，那么就可以对返回值进行强制转换，然后返回
     * 当然，如果类型不匹配的话，会抛出运行时错误，强制转换失败 - java.lang.ClassCastException
     * 无法直接使用 System.out.println 打印
     * System.out.println(entity1.getData());  // 编译不通过
     * System.out.println((char[]) entity1.getData());  // 编译通过，运行不通过
     */
    public <T> T getData() {
        return (T) data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static void main(String[] args) {
        GenericsEntity1 entity1 = new GenericsEntity1();
        entity1.setData("张志刚");
        String dataStr = entity1.getData();
        System.out.println("dataStr = " + dataStr);
//        System.out.println(entity1.getData());            // 编译不通过
//        System.out.println((char[]) entity1.getData());   // 编译通过，运行不通过
        Object object = entity1.getData();
        System.out.println("object = " + object);

        entity1.setData(123);
        int dataInt = entity1.getData();
        System.out.println("dataInt = " + dataInt);

        entity1.setData(Arrays.asList("1,2,3,4,56".split(",")));
        List<String> dataList = entity1.getData();
        System.out.println("dataList = " + dataList);

        List<Teacher> srcList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Teacher teacher = new Teacher();
            teacher.setId(new Random().nextInt(100) + 5);
            srcList.add(teacher);
        }
        entity1.setData(srcList);
        List<Teacher> dataTeacherList = entity1.getData();
        System.out.println("dataTeacherList = " + dataTeacherList);

    }


}
