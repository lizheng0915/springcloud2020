package com.zzg.base.generics.entity;

import java.io.Serializable;

/**
 * 自定义类附带泛型
 * 1. 在类上使用<T>表示声明泛型，那么在后面的方法中就可以直接使用
 * 2. 如果不在类上面声明，那么需要在使用的每一个方法上面进行声明，参考 GenericsEntity1.class
 * <p>
 * https://mp.weixin.qq.com/s/8xRysUHG6L2_s1qmJ4tSgw
 */
public class GenericsEntity<T> implements Serializable {

    private Integer code;
    private String message;
    private T data;

    /**
     * 提供多种构造方法
     */
    public GenericsEntity() {
        super();
    }

    public GenericsEntity(int code) {
        super();
        this.code = code;
    }

    public GenericsEntity(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public GenericsEntity(int code, String message, T data) {
        super();
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * getter/setter方法
     */
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
