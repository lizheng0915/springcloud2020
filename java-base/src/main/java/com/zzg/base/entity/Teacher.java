package com.zzg.base.entity;


import java.io.Serializable;

public class Teacher implements Serializable {

    private Integer id;

    private String name;

    /**
     * transient 修饰的属性不会被序列化
     */
    private transient String password;

    public Teacher() {
    }

    public Teacher(Integer id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
