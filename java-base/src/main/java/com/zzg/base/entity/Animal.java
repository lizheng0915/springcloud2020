package com.zzg.base.entity;

import java.util.Date;

public abstract class Animal implements IAnimal {

    private String name;

    private Integer age;

    private Date birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * 1. 抽象方法不需要具体的实现
     * 2. 抽象方法必须使用 abstract 修饰
     * 3. 包含抽象方法的类必须是抽象类
     * 4. 抽象类中的抽象方法的修饰符只能为public或者protected，默认为public；
     */
    public abstract void eat(String food);

    public String printClassAndName() {
        String res = String.format("class = %s; name = %s", this.getClass(), name);
        System.out.println(res);
        return res;
    }

    public static void animalStaticMethod() {
        System.out.println("This is Animal abstract class Static Method");
    }
}
