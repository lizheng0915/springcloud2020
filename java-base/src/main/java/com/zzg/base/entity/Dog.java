package com.zzg.base.entity;

public class Dog extends Animal {


    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = "旺财 : " + name;
    }

    @Override
    public void eat(String food) {
        if ("bone".equalsIgnoreCase(food)) {
            System.out.println("So Happy: Dog like eat bone");
        } else {
            System.out.println("Unhappy : Dog do not like eat " + food);
        }
    }

    @Override
    public String printClassAndName() {
        String res = String.format("class = %s; name = %s", this.getClass(), name);
        System.out.println(res);
        return res;
    }

}
