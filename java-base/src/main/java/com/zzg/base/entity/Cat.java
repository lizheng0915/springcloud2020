package com.zzg.base.entity;

public class Cat extends Animal {


    @Override
    public void eat(String food) {
        if ("fish".equalsIgnoreCase(food)) {
            System.out.println("So Happy: Dog like eat fish");
        } else {
            System.out.println("Unhappy : Dog do not like eat " + food);
        }
    }
}
