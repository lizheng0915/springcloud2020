package com.zzg.base.environment;


import java.util.Map;

/**
 * 1. 通过 System.getenv(); 来获取系统环境变量；环境变量相当于操作系统属性，所有程序均可使用。不仅仅是JAVA程序
 * 2. 只能获取系统用户的环境变量，给单独用户配置的不生效
 * 3. 在系统运作是不可修改 - 只有get方法，没有对应的set方法，表示无法通过JAVA程序来设置系统的环境变量
 * 4. 操作系统内部所有程序均有效，不仅仅针对JAVA程序
 * 5. 有一部分是用户自己设置的，比如 JAVA_HOME、 HADOOP_HOME、 USERNAME、 COMPUTERNAME 等
 * 6. 有一部分是硬件环境，比如 : NUMBER_OF_PROCESSORS （CPU逻辑处理器个数）、OS（操作系统）等
 * 7. 部分环境变量解释 - https://blog.csdn.net/df0128/article/details/90485215/
 */
public class EnvDemo {

    private static final String format = "envs : key = %-30s  value = %s";

    public static void main(String[] args) throws InterruptedException {

        System.out.println("==========System.getenv()==========");
        // System.getenv(); 获取系统的所有环境变量
        Map<String, String> envs = System.getenv();
        for (String key : envs.keySet()) {
            System.out.println(String.format(format, key, envs.get(key)));
        }

        System.out.println("==========System.getenv(key)==========");
        // 根据key获取具体的环境变量值
        String key = "HADOOP_HOME";
        String value = System.getenv(key);
        System.out.println(String.format(format, key, value));

        // 在window环境下配置用户的环境变量 USER_TEST_HOME - 返回结果为 null
        key = "USER_TEST_HOME";
        value = System.getenv(key);
        System.out.println(String.format(format, key, value));

    }

}
