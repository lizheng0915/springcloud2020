package com.zzg.base.number;


import java.math.BigDecimal;

/**
 * 众所周知，由于java存储数据的方式不同，因此在java中对非常精确的小数做运算时，得到的结果可能是不精确的 - 参看下面方法 floatDataCompute
 * 在商业运算中，这点微小的误差有可能造成非常严重的后果。
 * 所以在商业应用开发中，涉及金额等浮点数计算的数据，全部使用BigDecimal进行加减乘除计算
 * <p>
 * 2. BigDecimal 的构造方法 -
 * public BigDecimal(String val) { .... }  --> 使用字符串当参数的构造方法使用比较多，因为字符串描述小数时可以非常精确
 * public BigDecimal(double val) { .... }  --> 使用double当参数的构造方法使用比较少，因为double本身描述的就不够精确，传给BigDecimal的源头就是错误的
 * <p>
 * 3. 偏移量取值规则
 * CEILING    :  向正无限大方向舍入的舍入模式 - 天花板 - 坐标轴向左
 * DOWN       :  向零方向舍入的舍入模式 - 0 - 坐标轴中间
 * UP         :  向远离零方向舍入的舍入模式 - 0 - 向坐标轴两端
 * FLOOR      :  向负无限大方向舍入的舍入模式 - 地板  -  坐标轴向右
 * HALF_DOWN  :  四舍五入的舍入模式 - 如果与两个相邻数字的距离一样，则向下舍入; 1.235 --> 1.23
 * HALF_UP    :  四舍五入的舍入模式 - 如果与两个相邻数字的距离一样，则向上舍入; 1.235 --> 1.24
 * HALF_EVEN  :  四舍五入的舍入模式 - 如果与两个相邻数字的距离一样，则向相邻的偶数舍入; 1.235 --> 1.24;  1.245 --> 1.24
 */
public class BigDecimalDemo {


    public static void main(String[] args) {
        /**
         * 数据格式化
         * 1. setScale() 设置保留小数点后面精度位数 和 使用的计算方法
         * 2. BigDecimal.ROUND_HALF_UP --> 四舍五入计算法  --> 看代码
         * 3. 下面示例代码中，如果创建BigDecimal的参数是 double 类型，则容易出现错误情况，究其原因就是因为double的精度丢失问题
         *
         * Rounding Modes -- Rounding 模式 -- 按照 scale = 2 的情况
         * ROUND_UP        : 向上取值  1.2400 --> 1.24; 1.2301 --> 1.24; 1.2367 --> 1.24;
         * ROUND_DOWN      : 向下取值  1.2400 --> 1.24; 1.2301 --> 1.23; 1.2367 --> 1.23;
         * ROUND_CEILING   : 如果数值是正数则跟ROUND_UP一样，如果是负数则跟ROUND_DOWN一样
         *                   1.2301 --> 1.24; -1.2301 --> -1.23
         * ROUND_FLOOR     : 跟ROUND_CEILING完全相反；如果数值是正数则跟ROUND_DOWN一样，绝对值向下取值；如果数值是负数则跟ROUND_UP一样，绝对值向上取值
         *                   1.2301 --> 1.23; -1.2301 --> -1.24
         * ROUND_HALF_UP   : 常用模式，四舍五入取值（.5向上）; 只要小数点精度后面的一位 >= .5 则向上取值，否则向下取值
         *                   1.2349 --> 1.23; 1.235 --> 1.24; -1.2349 --> -1.23; -1.235 --> -1.24
         * ROUND_HALF_DOWN : 非常用模式，也是四舍五入取值，(.5向下); 也就是如果后面是 .5 则向下取值，跟平时的四舍五入不太一致
         *                   1.235 --> 1.23;  1.2349 --> 1.23
         * ROUND_HALF_EVEN : 未知使用场景
         * ROUND_UNNECESSARY: 未知使用场景
         */
        System.out.println(new BigDecimal("1.12400").setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());
        System.out.println(new BigDecimal("1.1249").setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());
        System.out.println(new BigDecimal("1.125").setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());
        System.out.println(new BigDecimal("1.135").setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());
        System.out.println(new BigDecimal("1.1251").setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());
        System.out.println(new BigDecimal("1.1267").setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());

    }


    public static void decimalDataCompute() {
        /**
         * BigDecimal 加减乘除的运算
         * 1. 创建BigDecimal对象
         * 2. add（加法） + subtract（减法） + multiply（乘法） + divide（除法）
         */
        System.out.println("==============================加减乘除的运算==============================");
        BigDecimal decimal1 = new BigDecimal("0.003");
        BigDecimal decimal2 = new BigDecimal("0.001");

        System.out.println("decimal1.add(decimal2) = " + decimal1.add(decimal2));
        System.out.println("decimal1.subtract(decimal2) = " + decimal1.subtract(decimal2));
        System.out.println("decimal2.subtract(decimal1) = " + decimal2.subtract(decimal1));
        System.out.println("decimal1.multiply(decimal2) = " + decimal1.multiply(decimal2));
        System.out.println("decimal1.divide(decimal2) = " + decimal1.divide(decimal2));
//        System.out.println("decimal2.divide(decimal1) = " + decimal2.divide(decimal1));  // 如果结果是无限小数结果，直接报错
    }


    public static void floatDataCompute() {
        System.out.println(0.05 + 0.01);    // 0.060000000000000005
        System.out.println(2.2 - 0.43);     // 1.7700000000000002
        System.out.println(5.0554 * 100);   // 505.53999999999996
        System.out.println(123.3 / 100);      // 1.2329999999999999
    }

}
