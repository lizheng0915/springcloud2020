package com.zzg.base.enums;

/**
 * 可以定义多个变量的枚举
 * 1. 必须提供构造方法
 * 2. 由于枚举对象的值不应该被修改，不能提供 setXXXX 方法，否则将修改全局的值
 * 3. 枚举的属性和构造方法必须放在定义的枚举后面
 * 4. toString 重写的方法，需要在每一个定义的枚举变量里面编写
 * 4.
 */
public enum WeekdayEnum {

    /**
     * 让枚举对象分别实现接口或者toString方法
     * MONDAY(1, "MON", "星期一") {
     *
     * @Override public String toString() {
     * return "toString : MONDAY";
     * }
     * },
     */

    MONDAY(1, "MON", "星期一"),
    TUESDAY(2, "TUE", "星期二"),
    WEDNESDAY(3, "WED", "星期三"),
    THURSDAY(4, "THU", "星期四"),
    FRIDAY(5, "FRI", "星期五"),
    SATURDAY(6, "SAT", "星期六"),
    SUNDAY(7, "SUN", "星期日");

    private int code;
    private String shortName;
    private String chineseName;

    /**
     * 多参数枚举必须提供一个私有构造方法，默认就是私有修饰符
     */
    WeekdayEnum(int code, String shortName, String chineseName) {
        this.code = code;
        this.shortName = shortName;
        this.chineseName = chineseName;
    }

    public int getCode() {
        return code;
    }

    public String getShortName() {
        return shortName;
    }

    public String getChineseName() {
        return chineseName;
    }

}
