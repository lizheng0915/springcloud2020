package com.zzg.base.date;

import java.time.LocalTime;
import java.time.temporal.ChronoField;

/**
 * LocalTime 跟 LocalDate 差不多，只是用于记录了具体时间内容，没有包含 年月日等信息
 * 1. 默认格式是 HH:mm:ss.SSS
 */
public class LocalTimeDemo {

    public static void main(String[] args) {
        // 通过时、分、秒创建一个时间
        LocalTime time1 = LocalTime.of(12, 12, 12);
        System.out.println("time1 = " + time1);

        LocalTime currentTime = LocalTime.now();
        System.out.println("currentTime = " + currentTime);

        // 获取时、分、秒
        System.out.println("currentTime.getHour() = " + currentTime.getHour());
        System.out.println("currentTime.get(ChronoField.HOUR_OF_DAY) = " + currentTime.get(ChronoField.HOUR_OF_DAY));
        System.out.println("currentTime.getMinute() = " + currentTime.getMinute());
        System.out.println("currentTime.get(ChronoField.MINUTE_OF_HOUR) = " + currentTime.get(ChronoField.MINUTE_OF_HOUR));
        System.out.println("currentTime.getSecond() = " + currentTime.getSecond());
        System.out.println("currentTime.get(ChronoField.SECOND_OF_MINUTE) = " + currentTime.get(ChronoField.SECOND_OF_MINUTE));

        // 时间加减
        System.out.println("====================时间加减=================");
        System.out.println("currentTime.plusHours(10) = " + currentTime.plusHours(10));
        System.out.println("currentTime.plusMinutes(11) = " + currentTime.plusMinutes(11));
    }

}
