
## 为什么JDK1.8重新引入新的时间控制API
1. Date 如果不进行格式化，打印出来的格式非常不容易阅读
2. 可以使用SimpleDateFormat对时间进行格式化，但是SimpleDateFormat的方法不是线程安全的方法，在多线程的情况下容易出问题
3. Calendar 是共享变量，并且这个共享变量没有做线程安全的控制。
4. Date对时间处理比较麻烦，比如想获取某年、某月、某星期，以及n天以后的时间
5. 跟数据库的时间类型无法对标，date是一个距离某一个时间的毫秒值的偏移量，用来记录时间是需要进行转换的。

## 多线程并发如何保证线程安全
1. 避免线程之间共享一个SimpleDateFormat对象，每个线程使用时都创建一次SimpleDateFormat对象 => 创建和销毁对象的开销大
2. 对使用format和parse方法的地方进行加锁 => 线程阻塞性能差
3. 使用ThreadLocal保证每个线程最多只创建一次SimpleDateFormat对象 => 较好的方法

## java.time 包下的所有类都是不可变类型，并且线程安全
1. Instant 瞬时实例。
2. LocalDate 本地日期，不包含具体时间 例如：2014-01-14 可以用来记录生日、纪念日、加盟日等。
3. LocalTime 本地时间，不包含日期。
4. LocalDateTime 组合了日期和时间，但不包含时差和时区信息。
5. ZonedDateTime 最完整的日期时间，包含时区和相对UTC或格林威治的时差。

新API还引入了 ZoneOffSet 和 ZoneId 类，使得解决时区问题更为简便。解析、格式化时间的 DateTimeFormatter 类也全部重新设计。


