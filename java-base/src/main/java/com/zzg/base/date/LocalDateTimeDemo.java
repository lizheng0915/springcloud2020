package com.zzg.base.date;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * LocalDateTime = LocalDate + LocalTime
 * 1. 默认格式 2022-05-02T23:49:24.800
 * 2. 其他API跟LocalDate和LocalTime一样
 */
public class LocalDateTimeDemo {

    public static void main(String[] args) {

        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("localDateTime = " + localDateTime);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println("localDateTime.format(formatter) = " + localDateTime.format(formatter));

    }

}
