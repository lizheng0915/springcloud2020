package com.zzg.base.date;


import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalUnit;

/**
 * 参考 : https://blog.csdn.net/qq_31635851/article/details/117880835
 * 1. 在Java 8中引入的LocalDate表示一个格式为yyyy-MM-dd的日期
 * 2. 它不存储时间或时区。
 * 3. LocalDate是一个不可变的类，它是对日期的描述，如生日。
 * 4. LocalDate是一个基于值的类，要比较LocalDate的两个实例，我们应该使用它的equals 方法。
 * 5. LocalDate的格式可以通过DateTimeFormatter的格式方法来改变。
 * 6. 现在LocalDate的方法有：atTime、format、getDayOfMonth、getDayOfWeek、minus、plus、equals、compareTo等。
 */
public class LocalDateDemo {

    public static void main(String[] args) {

        createLocalDate();

        // LocalDate 的默认格式是 : yyyy-MM-dd

        // now(): 给出LocalDate实例，该实例包含默认时区的系统时钟的当前日期。
        LocalDate now = LocalDate.now();
        System.out.println("now = " + now);

        LocalDate today = LocalDate.now();
        System.out.println("today = " + today);

        // 获取年、月、日、星期
        System.out.println("today.getYear() = " + today.getYear());
        System.out.println("today.get(ChronoField.YEAR) = " + today.get(ChronoField.YEAR));

        Month month = today.getMonth();
        System.out.println("month = " + month);
        int monthValue = today.getMonthValue();
        System.out.println("monthValue = " + monthValue);
        System.out.println("today.get(ChronoField.MONTH_OF_YEAR) = " + today.get(ChronoField.MONTH_OF_YEAR));

        System.out.println("today.getDayOfMonth() = " + today.getDayOfMonth());
        System.out.println("today.get(ChronoField.DAY_OF_MONTH) = " + today.get(ChronoField.DAY_OF_MONTH));

        System.out.println("today.getDayOfWeek() = " + today.getDayOfWeek());
        System.out.println("today.get(ChronoField.DAY_OF_WEEK) = " + today.get(ChronoField.DAY_OF_WEEK));


        // 判断两个日期是否相等 - 需要使用 equals 方法 - 如果比较的日期是字符型的，需要先解析成日期对象再作判断。
        LocalDate localDate1 = LocalDate.now();
        LocalDate localDate2 = LocalDate.of(2022, 5, 2);
        System.out.println("localDate1 = " + localDate1);
        System.out.println("localDate2 = " + localDate2);
        System.out.println(localDate1.equals(localDate2));

        // 检查像生日这种周期性事件 - 比如是判断今天是不是 5月20等
        // MonthDay 类组合了月份和日期，去掉了年份，可以用来判断每年都会发生的事情。
        // YearMonth 同理
        MonthDay birthday = MonthDay.of(5, 2);
        MonthDay currentMonthDay = MonthDay.from(today);
        if (birthday.equals(currentMonthDay)) {
            System.out.println("今天是你的生日~~~~~~~~~~");
        } else {
            System.out.println("今天不是你的生日");
        }

        // 日期加减 - 当参数值是负数的时候对应减法
        System.out.println("====================日期加减=================");
        System.out.println("today.plusDays(5) = " + today.plusDays(5));     // plus  加法，传入负数变减法
        System.out.println("today.minusDays(5) = " + today.minusDays(5));   // minus 减法，传入负数变加法
        System.out.println("today.plusWeeks(2) = " + today.plusWeeks(2));
        System.out.println("today.plusMonths(1) = " + today.plusMonths(1));
        System.out.println("today.plusYears(5) = " + today.plusYears(5));


        // with 调整日期
        System.out.println("today.with(DayOfWeek.FRIDAY) = " + today.with(DayOfWeek.FRIDAY));  // 向后调整到星期五
        System.out.println("today.withYear(2011) = " + today.withYear(2011));
        System.out.println("today.withMonth(12) = " + today.withMonth(12));
        System.out.println("today.withDayOfMonth(12) = " + today.withDayOfMonth(12));
        System.out.println("today.withDayOfYear(100) = " + today.withDayOfYear(365 / 3));

        // 格式转换
        System.out.println("====================日期格式转换=================");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
        System.out.println(today.format(formatter));

        // 日期比较
        System.out.println("====================日期比较=================");
        LocalDate tempLocalDate = LocalDate.of(2011, 12, 12);
        System.out.println("today.isAfter(tempLocalDate) = " + today.isAfter(tempLocalDate));
        System.out.println("today.isBefore(tempLocalDate) = " + today.isBefore(tempLocalDate));
        System.out.println("today.isEqual(tempLocalDate) = " + today.isEqual(tempLocalDate));

        // 获取当前月的天数和当前年的天数 - 月的天数取值范围: 28、 29、 30、 31; 年的天数取值范围: 365、 366
        System.out.println("today.lengthOfMonth() = " + today.lengthOfMonth());
        System.out.println("today.lengthOfYear() = " + today.lengthOfYear());


        // 日期计算
        System.out.println("====================日期计算=================");
        // 计算两个日期之间的天数和月数
        LocalDate computeLocalDate = LocalDate.of(2011, Month.DECEMBER, 15);
        Period mathPeriod = Period.between(computeLocalDate, today);   // 获取 computeLocalDate --> today 的时间间隔
        System.out.println("mathPeriod.getYears() = " + mathPeriod.getYears());
        System.out.println("mathPeriod.getMonths() = " + mathPeriod.getMonths());
        System.out.println("mathPeriod.getDays() = " + mathPeriod.getDays());

    }


    public static void createLocalDate() {

        // 构造指定的年月日 - of 方法接受的三个参数，表示年、月、日
        LocalDate localDate = LocalDate.of(2021, 11, 12);
        System.out.println("localDate1 = " + localDate);

        // 从给定的纪元日数中给出LocalDate实例。 - 距离 1970-01-01 500天的后的日期
        System.out.println("LocalDate.ofEpochDay(500) = " + LocalDate.ofEpochDay(500));

        // 从给定的年份和年份中的天数给出LocalDate实例，输入数据类型为int。 - 2021年的第300天的日期
        System.out.println("LocalDate.ofYearDay(2021, 300) = " + LocalDate.ofYearDay(2021, 300));

        // 从给定的文本字符串，如 "2018-10-01"，给出LocalDate实例。
        localDate = LocalDate.parse("2018-09-09");
        System.out.println("localDate = " + localDate);

        // 从给定格式的文本字符串中获得LocalDate实例。输出的LocalDate将是yyyy-MM-dd格式。
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
        localDate = LocalDate.parse("2011年11月11日", formatter);
        System.out.println(localDate);

        System.out.println("==========createLocalDate==========");
    }


}
